let data = {
  _id: '616e96c270ecb47875d814e3',
  accessToken:
    'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzQ2Mzc1MDYsImV4cCI6MTY2NjE3MzUwNn0.jzHClaxtbJQTlXzzVL5LD1TSWwmJ-1AbhycHl7exMm6Sn24ySBa-wiX3f1sKgcggpUsoaBRANHcetVtsWrNZfA',
  dbRole: [
    {
      __v: 1,
      _id: '614eb8985ad2cfe88a4be6eb',
      createdAt: '2021-09-25T05:50:16.481Z',
      name: 'user',
      updatedAt: '2021-10-19T09:43:31.377Z',
      users: [Array],
    },
  ],
  dbUser: {
    __v: 0,
    _id: '616e96c270ecb47875d814e3',
    createdAt: '2021-10-19T09:58:26.658Z',
    email: 'a1@gmail.com',
    mobile: null,
    name: 'a1',
    password: '$2a$10$M/j3Hjcqb6R5J3vOKDASJe2IuioLLyfi3ttnuv85oWOHEmS3mK4Ha',
    roles: ['614eb8985ad2cfe88a4be6eb'],
    updatedAt: '2021-10-19T09:58:26.658Z',
    username: 'a1',
  },
  message: 'User was registered successfully!',
  roles: ['ROLE_USER'],
  success: true,
  user: {
    email: 'a1@gmail.com',
    name: 'a1',
    password: '$2a$10$M/j3Hjcqb6R5J3vOKDASJe2IuioLLyfi3ttnuv85oWOHEmS3mK4Ha',
    username: 'a1',
  },
};
console.log('Mujhe kya chahiye >> ');
console.log('ID >>> ', data._id);
console.log('accessToken >>> ', data.accessToken);
