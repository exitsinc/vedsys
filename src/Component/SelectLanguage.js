import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  Alert,
  CheckBox,
  ImageBackground,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
const SelectLanguage = ({contact}) => {
  return (
    <View>
      <View style={{backgroundColor: 'white', margin: 20}}>
        <Text
          style={{
            textAlign: 'left',
            fontSize: 25,
            fontWeight: 'bold',
            color: 'black',
            marginBottom: 25,
          }}>
          {/* अपनी भाषा सेट करें */}
          Set Your Language
        </Text>
        <Text
          style={{
            fontSize: 18,
            color: 'green',
            marginBottom: 10,
          }}>
          English
        </Text>
        <Text
          style={{
            fontSize: 18,
            color: 'black',
            marginBottom: 10,
          }}>
          Hindi - हिंदी
        </Text>

        <Text
          style={{
            fontSize: 18,
            color: 'black',
            marginBottom: 10,
          }}>
          Telugu - తెలుగు
        </Text>

        <Text
          style={{
            fontSize: 18,
            color: 'black',
            marginBottom: 10,
          }}>
          Tamil - தமிழ்
        </Text>

        <Text
          style={{
            fontSize: 18,
            color: 'black',
            marginBottom: 10,
          }}>
          Malayalam - മലയാളം
        </Text>

        <Text
          style={{
            fontSize: 18,
            color: 'black',
            marginBottom: 10,
          }}>
          kannada - ಕನ್ನಡ
        </Text>

        <Text
          style={{
            fontSize: 18,

            color: 'black',
            marginBottom: 10,
          }}>
          Marathi - मराठी
        </Text>

        <Text
          style={{
            fontSize: 18,

            color: 'black',
            marginBottom: 10,
          }}>
          Odia - ଓଡ଼ିଆ
        </Text>

        <Text
          style={{
            fontSize: 18,

            color: 'black',
            marginBottom: 10,
          }}>
          Bangla - বাংলা
        </Text>

        <Text
          style={{
            fontSize: 18,

            color: 'black',
            marginBottom: 10,
          }}>
          Arabic - عربى
        </Text>
      </View>
      <View
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          style={{
            backgroundColor: 'white',
            padding: 5,
            width: 150,
            justifyContent: 'center',
            alignSelf: 'center',
            borderRadius: 30,
            marginBottom: 10,
          }}
          onPress="Cancel">
          <Text
            style={{
              color: 'black',
              fontSize: 20,
              textAlign: 'center',
              marginBottom: 10,
              borderWidth: 2,
              borderRadius: 30,
              padding: 6,
            }}>
            Cancel
          </Text>
        </TouchableOpacity>
        <View style={{marginBottom: 10}}>
          <TouchableOpacity
            style={{
              backgroundColor: '#000',
              width: 150,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 30,
              marginBottom: 10,
            }}
            onPress={contact}>
            <Text
              style={{
                color: '#fff',
                fontSize: 20,
                textAlign: 'center',
                marginBottom: 10,
                borderWidth: 2,
                borderRadius: 30,
                paddingTop: 5,
              }}>
              Proceed
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default SelectLanguage;
