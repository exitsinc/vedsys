import ActionSheet from 'react-native-actions-sheet';
import React, {useState, createRef} from 'react';
import {
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  Alert,
  ScrollView,
} from 'react-native';
import {useEffect} from 'react';
import QuestionOne from './QuestionOne';
import axios from 'axios';
import ApiCalls from '../Api/Api';
import {Config} from '../../Config';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
// const actionSheetRef = createRef();
import {useDispatch, useSelector} from 'react-redux';
import {setIsLogged, setUser} from '../Redux/Slice/LoginSlice';
const call = new ApiCalls();
const SignUp = ({toggleActionSheet, setToggleActionSheet}) => {
  const dispatch = useDispatch();
  // let actionSheet;
  // const [Firstname, setFirstname] = React.useState('');
  // const [Lastname, setLastname] = React.useState('');
  // const [Country, setCountry] = React.useState('');
  // const [MobileNumber, setMobileNumber] = React.useState('');
  // const [Email, setEmail] = React.useState('');
  // const [Password, setPassword] = React.useState('');
  // const [Roles, setRoles] = React.useState('');
  // const [ConfirmPassword, setConfirmPassword] = React.useState('');

  const [loading, setloading] = useState(false);
  const [firstName, setfirstname] = useState('FirstName is required');
  const [lastName, setlastname] = useState('Lastname is required');
  const [emailerr, setemailerr] = useState('Email is required');
  const [passerr, setpasserr] = useState('Password is required');
  const [phoneerr, setphoneerr] = useState('Phone Number is required');
  const [countryCode, setcountrycode] = useState('Country Code is required');
  const [valid, setvalid] = useState(false);
  const [state, setstate] = useState({
    username: '',
    email: '',
    firstName: '',
    lastName: '',
    countryCode: '',
    phoneerr: '',
    password: '',
  });
  console.log('State User DATTA>>>', state);
  const [disabled, setDisabled] = useState(true);

  // console.log(emailerr, phoneerr, valid)
  let res = [];
  // let data = {
  //   username: Firstname,
  //   password: Password,
  //   email: Email,
  //   name: Firstname,
  //   roles: 'user',
  // };
  // console.log('DATA : ', data);
  const signupHandler = async () => {
    console.log('Hello');

    // axios
    //   .post('http://13.232.211.114:3000/api/auth/signup', data)
    //   .then(response => {
    //     console.log('response.data >> ', response.data);
    //     if (response.data.success == true) {
    //       // Alert.alert(`${response.data.success}`)
    //       setToggleActionSheet(3);
    //       console.log('signup successfully!!!!');
    //      // dispatch(setUser(response.data));
    //     } else {
    //       Alert.alert(`Please End Valid Details!!!`);
    //       // setToggleActionSheet(3);
    //     }
    //   })
    //   .catch(error => {
    //     console.error('There was an error!', error);
    //     Alert.alert(`Failed to signup!!!`);
    //   });
    // setToggleActionSheet(3);
    setvalid(true);
    try {
      setloading(true);

      console.log('Button Hit!!');
      const data = {
        username: state.username,
        email: state.email.toLowerCase(),
        firstName: state.firstName,
        lastName: state.lastName,
        password: state.password,
        countryCode: state.countryCode,
        roles: 'user',
      };
      console.log('DATA>>', data);
      res = await call.Calls('auth/signup', 'POST', data, Config);
      if (res.status == '200') {
        // console.log(res)
        console.log('SignUp data Server>>>', res.data);
        dispatch(setUser(res.data));
        dispatch(setIsLogged(true));
        setToggleActionSheet(3);
        // navigation.replace(route.params.screen);
      } else {
        // console.log(res)
        // console.log(res.msg.response.data.message);
        //alert(res.msg.response.data.message);
        alert(`Please Enter Valid Details!!!`);
      }

      // console.log(state)
      // console.log("res", res,)
      setloading(false);
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };
  // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  const Validate = (e, msg) => {
    // console.log(e, msg)
    const regexname = new RegExp(/^[a-zA-Z ]+$/);
    const regexphone = new RegExp(/^[0-9]{10}$/);
    const regexEmail = new RegExp(
      /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/,
    );
    // const regexPhone = new RegExp(/^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/);
    switch (msg) {
      case 'firstname':
        if (e.length === 0) {
          setfirstname('FirstName is required');
        } else if (!regexname.test(e)) {
          setfirstname('Invalid first name');
        } else {
          setfirstname();
        }
        break;
      case 'lastname':
        if (e.length === 0) {
          setlastname('LastName is required');
        } else if (!regexname.test(e)) {
          setlastname('Invalid Last name');
        } else {
          setlastname();
        }
        break;
      case 'email':
        if (e.length === 0) {
          setemailerr('Email is required');
        } else if (!regexEmail.test(e)) {
          setemailerr('Invalid email');
        }
        // else if (regexEmail.test(e)) {
        //   HandleEmail(e)
        // }
        else {
          setemailerr();
        }
        break;
      case 'pass':
        if (e.length === 0) {
          setpasserr('Password is required ');
        } else if (e.length <= 5) {
          setpasserr("'Password is too small");
        } else {
          setpasserr();
        }
        break;
      case 'phone':
        if (e.length === 0) {
          setphoneerr('Phone Number is required ');
        } else if (!regexphone.test(e)) {
          setphoneerr('Invalid phone Number');
        } else {
          setphoneerr();
        }
        break;

      case 'countrycode':
        if (e.length === 0) {
          Alert.alert(`Please Enter Country Code !`);
        }
        // else if (e.length != 0) {
        //   HandleUsername(e)
        // }
        else {
          setcountrycode();
        }
        break;
    }
  };

  return (
    <View style={{height: responsiveHeight(80)}}>
      <ScrollView>
        {/* <ActionSheet ref={actionSheetRef}> */}
        <View
          style={{
            backgroundColor: 'white',
            padding: 20,
            borderRadius: 22,
          }}>
          <Text
            style={{
              textAlign: 'left',
              fontSize: 25,
              fontWeight: 'bold',
              color: '#000',
              Padding: 10,
            }}>
            Sign Up
          </Text>
          <Text
            style={{
              textAlign: 'left',
              fontSize: 10,
              fontWeight: 'bold',
              color: 'black',
              Padding: 10,
            }}>
            Already Have an account ?
            <Text style={{color: '#9CD161'}}>Login</Text>
          </Text>
          <TextInput
            placeholder="First Name"
            placeholderTextColor="#a7abb5"
            onChangeText={e => {
              Validate(e, 'firstname'), setstate({firstName: e});
            }}
            style={{
              color: '#a7abb5',
              marginBottom: 10,
              borderBottomColor: '#a7abb5',
              borderBottomWidth: 1,
              fontSize: 15,
            }}
            // value={Firstname}
          />
          {firstName && <Text style={{color: 'red'}}>{firstName}</Text>}
          <TextInput
            placeholder="Last Name"
            placeholderTextColor="#a7abb5"
            onChangeText={e => {
              Validate(e, 'lastname'), setstate({...state, lastName: e});
            }}
            style={{
              color: '#a7abb5',
              marginBottom: 10,
              borderBottomColor: '#a7abb5',
              borderBottomWidth: 1,
              fontSize: 15,
            }}
            // value={Lastname}
          />
          {lastName && <Text style={{color: 'red'}}>{lastName}</Text>}
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextInput
              placeholder="Country"
              placeholderTextColor="#a7abb5"
              onChangeText={e => {
                Validate(e, 'countrycode'),
                  setstate({...state, countryCode: e});
              }}
              style={{
                color: '#a7abb5',
                marginBottom: 10,
                borderBottomColor: '#a7abb5',
                borderBottomWidth: 1,
                flex: 0.2,
                fontSize: 15,
              }}
              // value={Country}
            />
            <TextInput
              keyboardType="numeric"
              placeholder="Mobile Number"
              placeholderTextColor="#a7abb5"
              onChangeText={e => {
                Validate(e, 'phone'),
                  setstate({...state, phoneerr: e, username: e});
              }}
              style={{
                color: '#a7abb5',
                marginBottom: 10,
                borderBottomColor: '#a7abb5',
                borderBottomWidth: 1,
                fontSize: 15,
                flex: 0.7,
              }}
              // value={MobileNumber}
            />
          </View>
          <View style={{alignItems: 'flex-end'}}>
            {phoneerr && <Text style={{color: 'red'}}>{phoneerr}</Text>}
          </View>
          <TextInput
            keyboardType="email-address"
            placeholder="Email"
            placeholderTextColor="#a7abb5"
            onChangeText={e => {
              Validate(e, 'email'), setstate({...state, email: e});
            }}
            style={{
              color: '#a7abb5',
              marginBottom: 10,
              borderBottomColor: '#a7abb5',
              borderBottomWidth: 1,
              fontSize: 15,
            }}
            // value={Email}
          />
          {emailerr && <Text style={{color: 'red'}}>{emailerr}</Text>}
          <TextInput
            secureTextEntry={true}
            placeholder="Password"
            placeholderTextColor="#a7abb5"
            onChangeText={e => {
              Validate(e, 'pass'), setstate({...state, password: e});
            }}
            style={{
              color: '#a7abb5',
              marginBottom: 10,
              borderBottomColor: '#a7abb5',
              borderBottomWidth: 1,
              fontSize: 15,
            }}
            // value={Password}
          />
          {passerr && <Text style={{color: 'red'}}>{passerr}</Text>}
          <TextInput
            secureTextEntry={true}
            placeholder="Confirm Password"
            placeholderTextColor="#a7abb5"
            onChangeText={e => {
              Validate(e, 'pass'), setstate({...state, password: e});
            }}
            style={{
              color: '#a7abb5',
              marginBottom: 10,
              borderBottomColor: '#a7abb5',
              borderBottomWidth: 1,
              fontSize: 15,
            }}
            // value={ConfirmPassword}
          />

          <TouchableOpacity
            // onPress={() => navigation.replace('Register')}
            //disabled={disabled}
            style={{
              backgroundColor: 'black',
              padding: 5,
              width: 200,
              justifyContent: 'center',
              alignSelf: 'center',
              borderRadius: 30,
              height: responsiveHeight(5),
              marginBottom: 10,
            }}
            onPress={() => signupHandler()}>
            <Text
              style={{
                color: 'white',
                fontSize: 15,
                fontWeight: 'bold',
                textAlign: 'center',
                textAlign: 'center',
                display: 'flex',
                justifyContent: 'center',
              }}>
              Sign Up
            </Text>
          </TouchableOpacity>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 16,
              fontWeight: 'bold',
              color: 'black',
              Padding: 10,
              marginBottom: 5,
              color: '#a7abb5',
            }}>
            <Text style={{color: 'black'}}>------------</Text>
            Or Sign up with<Text style={{color: 'black'}}>----------</Text>
          </Text>
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: 5,
              }}>
              <Image
                // style={styles.tinyLogo}
                source={require('../Images/search.png')}
                style={{height: 25, width: 25, margin: 15}}
              />
              <Image
                // style={styles.tinyLogo}
                source={require('../Images/facebook.png')}
                style={{height: 25, width: 25, margin: 15}}
              />
            </View>
          </View>
        </View>
        {/* </ActionSheet> */}
      </ScrollView>
    </View>
  );
};

export default SignUp;
