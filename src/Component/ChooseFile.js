import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TouchableOpacity,
  Button,
  Image,
  Alert,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {PermissionsAndroid} from 'react-native';
import axios from 'axios';
//import {setUser} from '../Redux/Slice/LoginSlice';
import {useDispatch, useSelector} from 'react-redux';
import DocumentPicker from 'react-native-document-picker';
const ChooseFile = () => {
  const [imageUri, setimageUri] = useState(null);
  const [imageUriGallary, setimageUriGallary] = useState(null);
  const [singleFile, setSingleFile] = useState(null);
  const [multipleFile, setMultipleFile] = useState([]);

  const data = useSelector(reduxState => reduxState.login.user);

  let userID = data._id;
  console.log('UserID Data>>>', userID);
  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'App Camera Permission',
          message: 'App needs access to your camera ',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Camera permission given');
        // const options = {
        //   storageOptions: {
        //     path: 'images',
        //     mediaType: 'photo',
        //   },
        //   includeBase64: true,
        // };

        let options = {
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };

        launchCamera(options, response => {
          console.log('Response = ', response);
          if (response.didCancel) {
            console.log('User cancelled image picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            // You can also display the image using data:
            // const source = {uri: 'data:image/jpeg;base64,' + response.base64};
            //setimageUri(source);
            let source = {uri: response.assets[0]};
            // ADD THIS
            setimageUri(source.uri);
            console.log('Source of Image>>>>>', source);
            console.log('Source of Image URI>>>>>', source.uri.uri);
            //setimageUri(source.uri);
            let imageData = [
              {
                fileCopyUri: null,
                name: 'Prescription.jpeg',
                size: 57023,
                type: 'image/jpeg',
                uri: source.uri.uri,
              },
            ];
            uploadFile(imageData[0]);
            //uploadFile(source.uri.uri);
          }
        });
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };
  //   const openCamera = () => {
  //     const options = {
  //       storageOptions: {
  //         path: 'images',
  //         mediaType: 'photo',
  //       },
  //       includeBase64: true,
  //     };

  //     launchCamera(options, response => {
  //       console.log('Response = ', response);
  //       if (response.didCancel) {
  //         console.log('User cancelled image picker');
  //       } else if (response.error) {
  //         console.log('ImagePicker Error: ', response.error);
  //       } else if (response.customButton) {
  //         console.log('User tapped custom button: ', response.customButton);
  //       } else {
  //         // You can also display the image using data:
  //         const source = {uri: 'data:image/jpeg;base64,' + response.base64};
  //         //setimageUri(source);
  //       }
  //     });
  //   };

  const openGallery = () => {
    // const options = {
    //   storageOptions: {
    //     path: 'images',
    //     mediaType: 'photo',
    //   },
    //   noData: true,
    //   //includeBase64: true,
    // };

    let options = {
      title: 'You can choose one image',
      maxWidth: 256,
      maxHeight: 256,
      noData: true,
      mediaType: 'photo',
      storageOptions: {
        skipBackup: true,
      },
    };

    launchImageLibrary(options, response => {
      console.log('Response = ', response);
      console.log('response>>>>', response.assets[0].uri);
      console.log('typeof res : ' + typeof response);
      //console.log('URI>>>', response[0].uri);
      console.log('Image Details >> ', response.assets[0].uri);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button:', response.customButton);
      } else if (response.assets[0].uri) {
        let source = {uri: response.assets[0]};
        setimageUriGallary(source.uri);
        //  console.log('Image Details>>>', response.assets);
        console.log('Source of Image URI>>>>>', source.uri.uri);

        let imageData = [
          {
            fileCopyUri: null,
            name: 'Prescription.jpeg',
            size: 57023,
            type: 'image/jpeg',
            uri: source.uri.uri,
          },
        ];
        uploadFile(imageData[0]);

        //uploadFile(response);
      } else {
        // You can also display the image using data:
        //  const source = {uri: 'data:image/jpeg;base64,' + response.base64};
        //console.log('Image details:>>>', source);
        // setimageUriGallary(source);
        console.log('NO image Selected');
      }
    });
  };
  const selectOneFile = async () => {
    //Opening Document Picker for selection of one file
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      //Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      console.log('typeof res : ' + typeof res);
      console.log('URI : ' + res[0].uri);
      console.log('Type : ' + res[0].type);
      console.log('File Name : ' + res[0].name);
      console.log('File Size : ' + res[0].size);
      //Setting the state to show single file attributes
      setSingleFile(res[0]);
      console.log('results>>', res);

      uploadFile(res[0]);
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        alert('Canceled File Selection');
      } else {
        //For Unknown Error
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const uploadFile = async data => {
    // Check if any file is selected or not
    console.log('Singlefile>>>', data);
    if (data != null) {
      // If file selected then create FormData
      const fileToUpload = data;
      const form = new FormData();

      form.append('files', fileToUpload);
      form.append('apiRoute', 'prescription');
      form.append('userId', userID);
      await axios({
        url: 'http://18.191.145.238:3001/api/upload',
        //url: 'http://13.232.211.114:3000/api/upload',
        method: 'POST',
        data: form,
        headers: {
          'Content-Type': 'multipart/form-data',
          // 'Access-Control-Allow-Origin': '*',
          // 'Access-Control-Allow-Credentials': true,
        },
      })
        .then(res => {
          console.log('res.data >>> ' + res.data);
          if (res.data) {
            alert('File Upload Successfully!');
          }
        })
        .catch(err => {
          alert(err);
          console.log('err >>> ' + err);
        });
    }
  };

  // const selectMultipleFile = async () => {
  //   //Opening Document Picker for selection of multiple file
  //   try {
  //     const results = await DocumentPicker.pickMultiple({
  //       type: [DocumentPicker.types.allFiles],
  //       //There can me more options as well find above
  //     });
  //     for (const res of results) {
  //       //Printing the log realted to the file
  //       console.log('res : ' + JSON.stringify(res));
  //       console.log('URI : ' + res.uri);
  //       console.log('Type : ' + res.type);
  //       console.log('File Name : ' + res.name);
  //       console.log('File Size : ' + res.size);
  //     }
  //     //Setting the state to show multiple file attributes
  //     setMultipleFile(results);
  //   } catch (err) {
  //     //Handling any exception (If any)
  //     if (DocumentPicker.isCancel(err)) {
  //       //If user canceled the document selection
  //       alert('Canceled from multiple doc picker');
  //     } else {
  //       //For Unknown Error
  //       alert('Unknown Error: ' + JSON.stringify(err));
  //       throw err;
  //     }
  //   }
  // };

  return (
    <View style={{height: vh(18), padding: 10}}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            margin: 10,
            paddingLeft: 20,
          }}>
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity
              onPress={() => {
                //openCamera();
                requestCameraPermission();
                //alert('OpenCamera');
                //Alert('hello');
              }}>
              <Image source={require('../assets/images/camera2.png')} />
              <Text>Camera</Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity
              onPress={() => {
                openGallery();
              }}>
              <Image source={require('../assets/images/gallery.png')} />
              <Text>Gallery</Text>
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'column'}}>
            <TouchableOpacity onPress={selectOneFile}>
              <Image
                source={require('../assets/images/folder.png')}
                style={{marginLeft: 10}}
              />
              <Text>File Manager</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ChooseFile;
