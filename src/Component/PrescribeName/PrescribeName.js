import ActionSheet from 'react-native-actions-sheet';
import React, {createRef} from 'react';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  CheckBox,
  ImageBackground,
  FlatList,
} from 'react-native';
import Filter from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
const actionSheetRef = createRef();
const PrescribeTab = [
  {
    id: 1,
    src: require('../../assets/images/medicine.png'),
    name: 'Prescription Name',
    date: 'Uploaded on Wed, Mar 17',
    // img1: 'source={require('../../Images/11.png')}',
  },
  {
    id: 2,
    src: require('../../assets/images/medicine.png'),
    name: 'Prescription Name',
    date: 'Uploaded on Wed, Mar 17',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 3,
    src: require('../../assets/images/medicine.png'),
    name: 'Prescription Name',
    date: 'Uploaded on Wed, Mar 17',
    // img: './Images/PlacementArea-29.png',
  },
];
const PrescribeName = () => {
  let actionSheet;

  return (
    <View style={{margin: 10}}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
        }}>
        <Text
          style={{
            fontSize: rf(2.2),
            fontFamily: 'Poppins',
            color: '#062816',
            fontWeight: 'bold',
          }}>
          Prescription
        </Text>
        <Filter name="close" size={25} color={'#707070'} />
      </View>
      <View style={{justifyContent: 'flex-end', flexDirection: 'row'}}>
        <TouchableOpacity
          style={{
            borderRadius: 40,
            borderWidth: 1.5,
            borderColor: '#9CD161',
            height: rh(5),
            width: rw(20),
            marginRight: 10,
          }}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 3,
              fontSize: rf(1.8),
              fontFamily: 'Poppins',
              color: '#9CD161',
            }}>
            + Add
          </Text>
        </TouchableOpacity>
      </View>

      <FlatList
        style={{margin: 10}}
        data={PrescribeTab}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <View
            style={{
              flexDirection: 'row',
              margin: 0,
              marginTop: 8,
              // padding: 10,
              height: rh(10),
              borderColor: '#D5DBDB',
              borderWidth: 1,
              borderRadius: 10,
              justifyContent: 'space-between',
            }}>
            <Image
              source={item.src}
              style={{
                height: rh(10),
                width: rw(22),
                borderTopLeftRadius: 10,
                borderBottomLeftRadius: 10,
              }}
            />
            <View
              style={{
                padding: 10,
                marginRight: 40,
                flexDirection: 'column',
                justifyContent: 'space-between',
              }}>
              <Text style={{fontSize: rf(2), fontWeight: 'bold'}}>
                {item.name}
              </Text>
              <Text style={{fontSize: rf(1.5)}}>{item.date}</Text>
            </View>
            <View style={{marginTop: 18, marginRight: 10}}>
              <Icon name="trash" size={20} color="#707070" />
            </View>
          </View>
          // </View>
        )}
      />
    </View>
  );
};

export default PrescribeName;
