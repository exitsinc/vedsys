import React, {createRef, useState} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Platform,
  Modal,
  FlatList,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
const notifydata = [
  {
    id: 1,
    src: require('../assets/images/oxycodone.png'),
    heading: 'Oxycodone Pill Time',
    subheading: 'Before Breakfast',
    time: '1min ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 2,
    src: require('../assets/images/xavier.png'),

    heading: 'Message from Xavier',
    subheading: 'Message description',
    time: '25mins ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 3,
    src: require('../assets/images/event.png'),
    heading: 'An Upcoming Event',
    subheading: 'Event Description',
    time: '55mins ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 4,
    src: require('../assets/images/pills.png'),
    heading: 'Vit C Pills Running Out',
    subheading: '3 out 0f 10',
    time: '3hrs ago ago',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 5,
    src: require('../assets/images/pills.png'),
    heading: 'Vit C Pills Running Out',
    subheading: '3 out 0f 10',
    time: '3hrs ago ago',
    // img: './Images/PlacementArea-29.png',
  },
];
const NotificationModal = props => {
  const {visibleData} = props;
  const [visible, setVisible] = useState(visibleData ? true : false);
  const showNotification = () => {
    setVisible(!visible);
  };
  React.useEffect(() => {
    setVisible(visibleData);
  }, [visibleData]);
  return (
    <Modal
      animationType={'fade'}
      transparent={true}
      visible={visible}
      onRequestClose={() => showNotification()}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#FFFFF',
          height: 300,
          width: '85%',
          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#fff',
          marginTop: 80,
          marginLeft: 30,
          elevation: 5,
        }}>
        <View
          style={{
            backgroundColor: 'white',
            height: 300,
            width: '100%',
            borderRadius: 10,
            //borderWidth: 1,
          }}>
          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
              padding: 10,
            }}>
            <Text>Recent Notifications</Text>
            <TouchableOpacity onPress={showNotification}>
              <Text style={{color: '#9CD161'}}>Close</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            style={{margin: 5}}
            data={notifydata}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View
                style={{
                  flexDirection: 'row',
                  padding: 10,
                  justifyContent: 'space-between',
                }}>
                <Image source={item.src} />
                <View
                  style={{
                    margin: 5,
                    flexDirection: 'column',
                  }}>
                  <Text style={{fontSize: vf(1.9), fontWeight: '700'}}>
                    {item.heading}
                  </Text>
                  <Text style={{fontSize: vf(1.5)}}>{item.subheading}</Text>
                </View>
                <Text
                  style={{
                    color: '#9FACA5',
                    fontSize: vf(1.3),
                    marginTop: 15,
                  }}>
                  {item.time}
                </Text>
              </View>
            )}
          />
        </View>
      </View>
    </Modal>
  );
};

export default NotificationModal;
