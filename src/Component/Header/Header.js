import React, {useState} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import Menu from 'react-native-vector-icons/Entypo';
import Mic from 'react-native-vector-icons/Feather';
import Notifications from 'react-native-vector-icons/Ionicons';
import MapIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Navigation from '../../Routes/Navigation';
import Screen from '../../Screens/lastscreen/Screen';
import {useSelector, useDispatch} from 'react-redux';
import DrawerContent from '../../Routes/Drawers/DrawerContent';
import {useNavigation} from '@react-navigation/native';

export default function Header({navigation, showNotification, props}) {
  //const navigation = useNavigation();

  console.log('Props>>>', navigation, showNotification);
  return (
    <View style={{padding: 10}}>
      <View
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View>
          <TouchableOpacity
          // onPress={() => {
          //   props.navigation.navigate('DrawerOpen');
          // }}
          // onPress={() => navigation.openDrawer()}
          // onPress={() => navigation.openDrawer}
          >
            <Menu name="menu" size={20} />
          </TouchableOpacity>
        </View>
        <View>
          <View
            style={{
              display: 'flex',
              justifyContent: 'space-evenly',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TouchableOpacity>
              <Mic name="mic" size={20} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: 10, marginRight: 10}}
              onPress={showNotification}>
              <Notifications name="notifications-outline" size={20} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('MyLocation')}>
              <MapIcon name="google-maps" size={25} style={{marginRight: 5}} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('Screen', {screen: 'Profile'})
              }>
              <Image
                source={{
                  uri: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                }}
                style={{width: 30, height: 30, borderRadius: 50}}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}
