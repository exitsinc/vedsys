import ActionSheet from 'react-native-actions-sheet';
import React, {createRef, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableHighlight,
  Text,
  TouchableOpacity,
  Button,
  Modal,
  View,
  Alert,
  CheckBox,
  ImageBackground,
} from 'react-native';
import {useEffect} from 'react';
import QuestionOne from './QuestionOne';
import axios from 'axios';
import {setUser, setIsLogged} from '../Redux/Slice/LoginSlice';
import ForgotPassword from '../Component/ForgotPassword';
import ApiCalls from '../Api/Api';
import {Config} from '../../Config';
import {useDispatch, useSelector} from 'react-redux';
const call = new ApiCalls();
import Fn from 'react-native-vector-icons/AntDesign';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
// const actionSheetRef = createRef();

const Login = ({
  toggleActionSheet,
  setToggleActionSheet,
  navigation,
  route,
}) => {
  // const [Username, setUsername] = React.useState('');
  // const [Password, setPassword] = React.useState('');
  const dispatch = useDispatch();
  const [loading, setloading] = useState(false);
  const [passerr, setpasserr] = useState('Password is required');
  const [username, setusername] = useState('Username is required');
  const [valid, setvalid] = useState(false);
  let res = [];
  const [state, setstate] = useState({
    username: '',
    password: '',
  });
  console.log('UserName && password>>', state.username, state.password);
  // let data = {
  //   username: Username,
  //   password: Password,
  // };
  //console.log('DATA : ', data);
  const loginHandler = async () => {
    console.log('Hello');
    // const data = {
    //   username: state.username,
    //   password: state.password,
    // };
    // axios
    //   .post('http://13.232.211.114:3000/api/auth/signin', data)
    //   .then(response => {
    //     console.log('response.data >> ', response.data);
    //     console.log('response.data.Success >> ', response.data.success);
    //     if (response.data.success == true) {
    //       // Alert.alert(`${response.data.success}`)
    //       navigation.push('Screen');
    //       dispatch(setUser(response.data));
    //       // dispatch(setIsLogged(true));
    //       console.log('login Successfully!!!');
    //     } else {
    //       Alert.alert(`wrong username or password!!!`);
    //     }
    //   })
    //   .catch(error => {
    //     console.error('There was an error!', error);
    //     Alert.alert(`wrong credentials`);
    //   });

    setvalid(true);
    try {
      setloading(true);
      // console.log(state)
      console.log('Button Hit!!');
      const data = {
        username: state.username,
        password: state.password,
      };
      console.log('Login DATA>>', data);
      res = await call.Calls('auth/signin', 'POST', data, Config);
      console.log('login res >> ', res);
      if (res.status == 200) {
        // console.log(res)
        dispatch(setUser(res.data));
        dispatch(setIsLogged(true));
        navigation.push('Screen');
        //navigation.goBack()
      } else {
        // console.log(res)
        console.log(res.msg.response.data.message);
        // alert(res.msg.response.data.message)
        alert('Wrong Username and Password');
      }
      // console.log("res", res,)
      setloading(false);
    } catch (e) {
      console.log(e);
      setloading(false);
    }
  };
  // let actionSheet;
  const actionSheetRef = createRef();
  //const [toggleActionSheet, setToggleActionSheet] = useState();
  const gotoForgotPassword = () => {
    setToggleActionSheet(15);
    actionSheetRef.current?.setModalVisible();
  };
  return (
    <>
      {/* <ActionSheet ref={actionSheetRef}> */}
      <View style={{backgroundColor: 'white', padding: 20, borderRadius: 20}}>
        <Text
          style={{
            textAlign: 'left',
            fontSize: 25,
            fontWeight: 'bold',
            color: 'black',
            Padding: 10,
          }}>
          Login
        </Text>
        <Text
          style={{
            textAlign: 'left',
            fontSize: 10,
            fontWeight: 'bold',
            color: 'black',
            Padding: 10,
          }}>
          New User ?
          <Text
            style={{
              textAlign: 'left',
              fontSize: 10,
              fontWeight: 'bold',
              color: '#9CD161',
              marginLeft: 10,
            }}>
            sign in
          </Text>
        </Text>

        <TextInput
          keyboardType="username-address"
          placeholder="Enter Username"
          placeholderTextColor="#a7abb5"
          onChangeText={e => {
            setstate({username: e});
          }}
          style={{
            color: '#a7abb5',
            marginBottom: 10,
            borderBottomColor: '#a7abb5',
            borderBottomWidth: 1,
            fontSize: 15,
          }}
          // value={state.username}
        />

        <TextInput
          secureTextEntry={true}
          placeholder="Enter Password"
          placeholderTextColor="#a7abb5"
          onChangeText={e => {
            setstate({...state, password: e});
          }}
          style={{
            color: '#a7abb5',
            marginBottom: 10,
            borderBottomColor: '#a7abb5',
            borderBottomWidth: 1,
            fontSize: 15,
          }}
          //  value={state.password}
        />
        <TouchableOpacity
          // onPress={() =>
          //   navigation.navigate('ForgotPassword')
          // }
          onPress={gotoForgotPassword}
          // style={{
          //   backgroundColor: 'black',
          //   padding: 5,
          //   width: 250,
          //   justifyContent: 'center',
          //   alignSelf: 'center',
          //   borderRadius: 30,
          //   marginBottom: 10,
          // }}
        >
          {/* onPress={forgetPasswordHandler}> */}
          <Text
            style={{
              textAlign: 'left',
              fontSize: 10,
              fontWeight: 'bold',
              color: '#9CD161',
              Padding: 10,
              marginBottom: 10,
            }}>
            Forgot Password ?
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          // onPress={() => navigation.replace('Register')}
          style={{
            backgroundColor: 'black',
            padding: 5,
            width: 250,
            justifyContent: 'center',
            alignSelf: 'center',
            alignItems: 'center',
            borderRadius: 30,
            marginBottom: 10,
            height: responsiveHeight(6),
          }}
          onPress={() => loginHandler()}>
          <Text
            style={{
              color: 'white',
              fontSize: 15,
              fontWeight: '600',
              // textAlign: 'center',
              // marginBottom: 10,
              display: 'flex',
              // justifyContent: 'center',
            }}>
            Login
          </Text>
        </TouchableOpacity>
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
            fontWeight: 'bold',
            color: 'black',
            Padding: 10,
            marginBottom: 5,
          }}>
          --------Or Login with-------
        </Text>
      </View>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 5,
          }}>
          <Image
            // style={styles.tinyLogo}
            source={require('../Images/search.png')}
            style={{height: 25, width: 25, margin: 15}}
          />
          <Image
            // style={styles.tinyLogo}
            source={require('../Images/facebook.png')}
            style={{height: 25, width: 25, margin: 15}}
          />
        </View>
      </View>
      {/* </ActionSheet> */}
      {/* {toggleActionSheet == 3 && (
        <QuestionOne toggleState={toggleActionSheet} />
      )} */}
      <ActionSheet ref={actionSheetRef}>
        {toggleActionSheet == 15 && (
          <ForgotPassword
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
      </ActionSheet>
    </>
  );
};

export default Login;
