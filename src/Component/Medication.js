import React, {createRef, useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Screen from '../Screens/lastscreen/Screen';
import {useDispatch, useSelector} from 'react-redux';
import {setUserHabits} from '../Redux/Slice/LoginSlice';
import ActionSheet from 'react-native-actions-sheet';
import ChooseFile from '../Component/ChooseFile';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import {Config} from '../../Config';
import axios from 'axios';
const Medication = ({
  // toggleActionSheet,
  //setToggleActionSheet,
  setShowScreen,
  navigation,
  route,
}) => {
  const dispatch = useDispatch();

  const actionSheetRef = createRef();
  const [toggleActionSheet, setToggleActionSheet] = useState();
  const gotoChooseFile = () => {
    setToggleActionSheet(1);
    actionSheetRef.current?.setModalVisible();
  };
  const gotoScreen = prescription => {
    console.log('go to Screen', navigation);

    dispatch(setUserHabits({prescription}));
    navigation.navigate('Screen');
    //   // setShowScreen(true);
    //
  };

  // useEffect(() => {
  //   const userHabitState = useSelector(
  //     reduxState => reduxState.login.userHabits,
  //   );
  //   const userDetails = useSelector(reduxState => reduxState.login.user);
  //   console.log('User Habit Details>>>', userHabitState);
  //   console.log('User  Details>>>', userDetails);
  //   console.log('Config >> ', Config);
  //   axios({
  //     // url: `${Config.PROTO}://${Config.IP}:${Config.PORT}/api/user/user/${userDetails._id}`,
  //     url: `http://192.168.0.104:8001/api/user/user/${userDetails._id}`,
  //     method: 'PUT',
  //     headers: {
  //       Authorization: `Bearer ${userDetails.accessToken}`,
  //     },
  //     data: userHabitState,
  //   })
  //     .then(res => {
  //       console.log(res.data);
  //     })
  //     .catch(err => {
  //       console.log(err);
  //     });
  // }, []);
  return (
    <View style={{height: vh(35), flexDirection: 'column', padding: 20}}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
        <View style={{flexDirection: 'column'}}>
          <Text
            style={{fontSize: vf(2.5), fontWeight: 'bold', color: '#06112D'}}>
            Are you Under
          </Text>
          <Text
            style={{fontSize: vf(2.5), fontWeight: 'bold', color: '#06112D'}}>
            Medication
          </Text>
          <Text style={{color: '#06112D'}}>
            If Please upload your prescription
          </Text>
        </View>

        <Icon name="close" size={25} />
      </View>
      <View
        style={{justifyContent: 'center', alignItems: 'center', marginTop: 15}}>
        <TouchableOpacity
          onPress={() => {
            gotoChooseFile();
          }}>
          <Image
            source={require('../assets/file-upload.png')}
            style={{height: vh(7.5), width: vw(13.3)}}
          />
          <Text style={{marginLeft: -8}}>Choose File</Text>
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
        <TouchableOpacity
          onPress={() => {
            gotoScreen(true);
          }}>
          <Text>Skip</Text>
        </TouchableOpacity>
      </View>
      <ActionSheet ref={actionSheetRef}>
        {toggleActionSheet == 1 && (
          <ChooseFile
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
      </ActionSheet>
    </View>
  );
};

export default Medication;
