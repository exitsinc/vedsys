import {createSlice} from '@reduxjs/toolkit';
const PharmacyData = [
  {
    id: 1,
    PharmacyName: 'Balaji Medicals',
    PharmacyAddress: 'Indira nagar 5th block Bangalore',
    PharmacyRating: '6 rating',
    PharmacyKm: '0.8KM',
    type: 'Chemists',
    homeDelivery: 'Chemist Home Delivery',
    src: require('../../Screens/FindMedicine/Medicine.png'),
  },
  {
    id: 2,
    PharmacyName: 'Mega Pharmacy',
    PharmacyAddress: 'Indira nagar 5th block Bangalore',
    PharmacyRating: '8 rating',
    PharmacyKm: '0.8KM',
    type: 'Chemists',
    homeDelivery: 'Chemist Home Delivery',
    src: require('../../assets/MedicaIimages/mega.png'),
  },
  {
    id: 3,
    PharmacyName: 'Mor Chemists',
    PharmacyAddress: 'Indira nagar 5th block Bangalore',
    PharmacyRating: '8 rating',
    PharmacyKm: '0.8KM',
    type: 'Chemists',
    homeDelivery: 'Chemist Home Delivery',
    src: require('../../assets/MedicaIimages/morchemist.png'),
  },
  {
    id: 4,
    PharmacyName: 'Sanjeevani Pharma',
    PharmacyAddress: 'Indira nagar 5th block Bangalore',
    PharmacyRating: '8 rating',
    PharmacyKm: '0.8KM',
    type: 'Chemists',
    homeDelivery: 'Chemist Home Delivery',
    src: require('../../assets/MedicaIimages/sanjeevani.png'),
  },
];

const CareTakerData = [
  {
    id: 1,
    careTakerName: 'Asha Kiran Home',
    careTakerAddress: 'Indira nagar 5th block Bangalore',
    careTakerRating: '6 rating',
    careTakerKm: '0.8KM',
    serviceTime: '24/7 Care taking Services',
    src: require('../../assets/CareTaker/ashakiran.png'),
  },
  {
    id: 2,
    careTakerName: 'Help Age',
    careTakerAddress: 'Indira nagar 5th block Bangalore',
    careTakerRating: '8 rating',
    careTakerKm: '1.8KM',
    serviceTime: '24/7 Care taking Services',
    src: require('../../assets/CareTaker/helpage.png'),
  },
  {
    id: 3,
    careTakerName: 'Manav Lok',
    careTakerAddress: 'Indira nagar 5th block Bangalore',
    careTakerRating: '8 rating',
    careTakerKm: '1.8KM',
    serviceTime: '24/7 Care taking Services',
    src: require('../../assets/care.png'),
  },
  {
    id: 4,
    careTakerName: 'Raksha Care',
    careTakerAddress: 'Indira nagar 5th block Bangalore',
    careTakerRating: '8 rating',
    careTakerKm: '1.8KM',
    serviceTime: '24/7 Care taking Services',
    src: require('../../assets/CareTaker/rakshacare.png'),
  },
];

const GroceryData = [
  {
    id: 1,
    groceryName: 'Balaji Departmental Stores',
    groceryAddress: 'Indira nagar 5th block Bangalore',
    groceryRating: '6 rating',
    groceryKm: '0.8KM',
    serviceTime: 'Self and Home Delivery Available',
    src: require('../../assets/Groceryimages/balaji.png'),
  },
  {
    id: 2,
    groceryName: 'Sivanandh Groceries',
    groceryAddress: 'Indira nagar 5th block Bangalore',
    groceryRating: '6 rating',
    groceryKm: '0.8KM',
    serviceTime: 'Self and Home Delivery Available',
    src: require('../../assets/Groceryimages/sivanandh.png'),
  },
  {
    id: 3,
    groceryName: 'Ginger Fresh',
    groceryAddress: 'Indira nagar 5th block Bangalore',
    groceryRating: '6 rating',
    groceryKm: '0.8KM',
    serviceTime: 'Self and Home Delivery Available',
    src: require('../../assets/Groceryimages/ginger.png'),
  },
  {
    id: 4,
    groceryName: 'Fresh Vegies',
    groceryAddress: 'Indira nagar 5th block Bangalore',
    groceryRating: '6 rating',
    groceryKm: '0.8KM',
    serviceTime: 'Self and Home Delivery Available',
    src: require('../../assets/Groceryimages/freshvegies.png'),
  },
];

const FoodServiceData = [
  {
    id: 1,
    foodServiceName: 'Hotel Amolodhbhavi',
    foodServiceAddress: 'Indira nagar 5th block Bangalore',
    foodServiceRating: '6 rating',
    foodServiceKm: '0.8KM',
    foodType: 'Veg Only',
    serviceTime: '24/7 Food Services',
    src: require('../../assets/FoodServiceimage/hotel1.png'),
  },

  {
    id: 2,
    foodServiceName: ' Hotel Karthikeya',
    foodServiceAddress: 'Indira nagar 5th block Bangalore',
    foodServiceRating: '6 rating',
    foodServiceKm: '0.8KM',
    foodType: 'Veg and NonVeg',
    serviceTime: '24/7 Food Services',
    src: require('../../assets/FoodServiceimage/hotel2.png'),
  },
  {
    id: 3,
    foodServiceName: 'Balaji Vegetarian Meals',
    foodServiceAddress: 'Indira nagar 5th block Bangalore',
    foodServiceRating: '6 rating',
    foodServiceKm: '0.8KM',
    foodType: 'Veg Only',
    serviceTime: '24/7 Food Services',
    src: require('../../assets/FoodServiceimage/hotel3.png'),
  },
  {
    id: 4,
    foodServiceName: 'Daspalla',
    foodServiceAddress: 'Indira nagar 5th block Bangalore',
    foodServiceRating: '6 rating',
    foodServiceKm: '0.8KM',
    foodType: 'Veg Only',
    serviceTime: '24/7 Food Services',
    src: require('../../assets/FoodServiceimage/hotel4.png'),
  },
];

const doctors = [
  {
    id: 1,
    src: require('../../assets/Doctor.png'),
    name: 'Dr. Meka Satyanarayana',
    type: 'General Physician. 11yrs exp',
    address: 'Indira nagar 5th Block Bangalore 0.8km',
  },
  {
    id: 2,
    src: require('../../assets/sonal.png'),
    name: 'Dr. Sonal Jain ',
    type: 'ENT Specialist. 06yrs exp',
    address: 'Indira nagar 5th Block Bangalore 0.8km',
  },
  {
    id: 3,
    src: require('../../assets/Sarika.png'),
    name: 'Dr. Sarika Samak',
    type: 'Diabetis Specialist. 03yrs exp',
    address: 'Indira nagar 5th Block Bangalore 0.8km',
  },
];

export const serviceslice = createSlice({
  name: 'services',
  initialState: {
    isLogged: false,
    coordinates: {
      lat: null,
      lng: null,
    },
    services: [],
    // pharmacyService: PharmacyData,
    // careTakerService: CareTakerData,
    // groceryService: GroceryData,
    // foodService: FoodServiceData,
    // doctorDetails: doctors,

    pharmacyService: [],
    careTakerService: [],
    groceryService: [],
    foodService: [],
    doctorService: [],
  },

  reducers: {
    setIsLogged: (state, action) => {
      state.isLogged = action.payload;
    },

    setCoordinates: (state, action) => {
      state.coordinates = action.payload;
    },

    setServices: (state, action) => {
      state.services = action.payload;
    },

    setPharmacyService: (state, action) => {
      state.pharmacyService = action.payload;
    },

    setCareTakerService: (state, action) => {
      state.careTakerService = action.payload;
    },

    setGroceryService: (state, action) => {
      state.groceryService = action.payload;
    },
    setFoodService: (state, action) => {
      state.foodService = action.payload;
    },
    setDoctorService: (state, action) => {
      state.doctorService = action.payload;
    },
  },
});

export const {
  setIsLogged,
  setCoordinates,
  setPharmacyService,
  setCareTakerService,
  setGroceryService,
  setFoodService,
  setServices,
  setDoctorService,
} = serviceslice.actions;

export default serviceslice.reducer;
