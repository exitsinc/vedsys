import {createSlice} from '@reduxjs/toolkit';

export const loginslice = createSlice({
  name: 'login',
  initialState: {
    isLogged: false,

    latlong: [],
    Location: [],
    user: [],
    address: [],
    userHabits: {
      smoke: false,
      walk: false,
      walkTimes: null,
      grocery: false,
      groceryTimes: null,
      prescription: false,
    },

    medicineDetails: [],
    categories: [],
    appointment: {
      symptoms: null,
      appointmentDate: null,
      department: null,
      appointmentTime: null,
      gender: null,
      hospitalName: null,
      doctorName: null,
    },
    stepCount: null,
    profilePic: null,
  },
  reducers: {
    setIsLogged: (state, action) => {
      state.isLogged = action.payload;
    },

    setCoordinates: (state, action) => {
      state.coordinates = action.payload;
    },
    setlatlong: (state, action) => {
      state.latlong = action.payload;
    },
    setLocation: (state, action) => {
      state.Location = action.payload;
    },

    setAddress: (state, action) => {
      state.address = action.payload;
    },

    setUser: (state, action) => {
      state.user = action.payload;
    },
    setCategories: (state, action) => {
      state.categories = action.payload;
    },
    setUserHabits: (state, action) => {
      console.log('setUserHabits', state, action);
      state.userHabits = {...state.userHabits, ...action.payload};
    },

    setMedicineDetails: (state, action) => {
      console.log('setMedicineDetails', state, action);
      //state.medicineDetails = action.payload;
      console.log('Type of Action>>', typeof action.payload, action.payload);
      //state.medicineDetails.push(action.payload);
      state.medicineDetails = [...state.medicineDetails, action.payload];
    },
    setappointment: (state, action) => {
      console.log('setAppointment', state, action);
      state.appointment = action.payload;
    },

    setStepCount: (state, action) => {
      state.stepCount = action.payload;
    },

    setProfilePic: (state, action) => {
      state.profilePic = action.payload;
    },
  },
});

//actions
export const {
  setIsLogged,
  setUser,
  setCoordinates,
  setAddress,
  setUserHabits,
  setappointment,
  setMedicineDetails,
  setLocation,
  setlatlong,
  setCategories,
  setStepCount,
  setProfilePic,
} = loginslice.actions;

export default loginslice.reducer;
