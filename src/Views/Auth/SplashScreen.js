import React from 'react';
import {
  Image,
  View,
  SafeAreaView,
  ImageBackground,
  Dimensions,
  Text,
  StyleSheet,
} from 'react-native';
//import {useSelector} from 'react-redux';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import {useDispatch, useSelector} from 'react-redux';
//import LocationEnabler from 'react-native-location-enabler';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import {setlatlong, setLocation} from '../../Redux/Slice/LoginSlice';
import {GeoDecoding} from '../../functions/GeoDecording';
import {setloading} from '../../Redux/Slice/HomeSlice';
import Geolocation from '@react-native-community/geolocation';
function SplashScreen({navigation}) {
  const login = useSelector(state => state.login);
  const home = useSelector(state => state.home);
  const {loading} = home;
  const dispatch = useDispatch();
  // let isLogged = true;
  // setTimeout(() => {
  //   if (isLogged) {
  //     navigation.replace('Home');
  //   } else {
  //     navigation.replace('Splash');
  //   }
  // }, 4000);

  React.useEffect(() => {
    setTimeout(
      () => {
        (async () => {
          dispatch(setloading(true));
          RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
            interval: 10000,
            fastInterval: 5000,
          })
            .then(async data => {
              await Geolocation.getCurrentPosition(
                position => {
                  console.log('Position -> ', position);
                  dispatch(setlatlong(position.coords));
                  GeoDecoding(
                    position.coords.latitude,
                    position.coords.longitude,
                    'AIzaSyBLMLoni4Ur29k6EHL1D_0NhHdBDwbyPlg',
                  )
                    .then(res => {
                      console.log(res);
                      dispatch(setLocation(res));
                      dispatch(setloading(false));
                      navigation.replace('Home');
                    })
                    .finally(() => dispatch(setloading(false)));
                },

                error => {
                  dispatch(setloading(false)),
                    navigation.replace('Home'),
                    console.log(error);
                },
                {enableHighAccuracy: false, timeout: 50000},
              );
            })
            .catch(err => {
              dispatch(setloading(false));
              console.log(loading);
              navigation.replace('Home');
            });
        })();
      },
      loading == false ? 2000 : null,
    );
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#c6e5f7',
        }}>
        <Image
          resizeMode="center"
          source={require('../../Images/Vedsys-Logo.png')}
          style={{
            width: responsiveWidth(80),
            height: '100%',
            // animation: 'fadeIn 5s',
            // transition: '650ms ease',
          }}
        />
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    padding: 15,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  contentCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    color: 'white',
    padding: 10,
  },
});
export default SplashScreen;
