import React, {useState, useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
// import FindMedicineItems from './FindMedicineItems';
import Header from '../../Component/Header/Header';
import NewBottomDemo from '../../Component/NewBottomDemo';
import {useDispatch, useSelector} from 'react-redux';
//import {setCategories} from '../../Redux/Slice/LoginSlice';
import {setloading} from '../../Redux/Slice/HomeSlice';
import ApiCalls from '../../Api/Api';
const call = new ApiCalls();
function FindMedicine({navigation, route}) {
  const dispatch = useDispatch();
  const data = useSelector(reduxState => reduxState.services);
  const Pharmacy_Data = useSelector(
    reduxState => reduxState.services.pharmacyService,
  );

  console.log('PharmacyData from redux >>>>>>>', Pharmacy_Data);

  return (
    <>
      <SafeAreaView>
        <View style={{height: '100%', width: '100%', backgroundColor: '#FFF'}}>
          <View
            style={{
              flex: 0.9,
            }}>
            <Header navigation={navigation} />
            <ScrollView showsVerticalScrollIndicator>
              <View style={styles.upper}>
                <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
                  Find Your Medicines
                </Text>
                <Icon name="filter-alt" size={30} color="#000" />
              </View>
              <View>
                <FlatList
                  style={{margin: 5}}
                  data={Pharmacy_Data}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item}) => {
                    console.log('item>>>', item);
                    console.log('item>>>', item.pic1);
                    return (
                      <View
                        style={{
                          flexDirection: 'row',
                          margin: 10,
                          //padding: 10,
                          borderColor: '#F3F6F6',
                          borderRadius: 10,
                          borderWidth: 1,
                          justifyContent: 'space-between',
                        }}>
                        <Image
                          source={{uri: item.pic1}}
                          style={{
                            width: 100,
                            height: 100,
                            borderTopLeftRadius: 10,
                            borderBottomLeftRadius: 10,
                          }}
                        />
                        <View
                          style={{
                            margin: 5,
                            flexDirection: 'column',
                          }}>
                          <Text style={{fontSize: vf(2.2), fontWeight: '700'}}>
                            {item.name}
                          </Text>
                          <Text style={{fontSize: vf(1.8)}}>
                            {item.address}
                          </Text>
                          <Text style={{fontSize: vf(1.8)}}>{item.rating}</Text>
                          <Text style={{fontSize: vf(1.8)}}>
                            {item.serviceType}
                          </Text>
                        </View>
                      </View>
                    );
                  }}
                />
              </View>
            </ScrollView>
          </View>
          <View
            style={{
              flex: 0.1,
              //  position: 'absolute',
              justifyContent: 'flex-end',
            }}>
            <NewBottomDemo />
          </View>
        </View>
      </SafeAreaView>
    </>
  );
    }
const styles = StyleSheet.create({
  // container: {margin: 10, marginTop: 30},
  upper: {
    marginLeft: 15,
    //  marginRight: 5,
    margin: 10,
    marginTop: 25,
    backgroundColor: '#FFFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  // list: {},
});
export default FindMedicine;
