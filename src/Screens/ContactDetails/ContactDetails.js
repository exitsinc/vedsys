import React from 'react';
import {TouchableOpacity} from 'react-native';
import {TextInput} from 'react-native';
import {
  StyleSheet,
  Button,
  View,
  SafeAreaView,
  Text,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Email from 'react-native-vector-icons/MaterialCommunityIcons';
import Phone from 'react-native-vector-icons/Feather';
import Message from 'react-native-vector-icons/Feather';
import Header from '../../Component/Header/Header';
import NewBottomDemo from '../../Component/NewBottomDemo';

// const Separator = () => <View style={styles.separator} />;

const ContactDetails = () => (
  <SafeAreaView>
      <Header />
    <View
      style={{
        justifyContent: 'flex-start',
        flexDirection: 'row',
        marginBottom: 10,
        alignItems: 'center',
      }}>
      <Text style={{fontWeight: 'bold', fontSize: 22, marginLeft: 20}}>
        Contact
      </Text>
    </View>

    <View
      style={{
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        paddingLeft:20
      }}>
      <View>
        <Email name="email-outline" size={30} color="#9cd191" />
      </View>
      <View style={{marginLeft: 10, marginTop: 20}}>
        <Text style={{fontWeight: 'bold', fontSize: 20, marginTop: 20}}>
          Email
        </Text>
        <TextInput
          style={{
            borderBottomWidth: 1,
            borderBottomColor: 'grey',
            paddingRight: 150,
            paddingLeft: 10,
          }}
          placeholder="Enter Your Email"
          keyboardType="alphanumeric"
        />
      </View>
    </View>

    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft:20

      }}>
      <View>
        <Phone name="phone" size={30} color="#9cd191" />
      </View>
      <View style={{marginLeft: 10, marginTop: 20}}>
        <Text style={{fontWeight: 'bold', fontSize: 20, marginTop: 20}}>
          Phone
        </Text>

        <TextInput
          style={{
            borderBottomWidth: 1,
            borderBottomColor: 'grey',
            paddingRight: 100,
          }}
          placeholder="Enter Your Phone Number"
          keyboardType="numeric"
        />
      </View>
    </View>

    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft:20
      }}>
      <View>
        <Message name="message-circle" size={30} color="#9cd191" />
      </View>
      <View style={{marginLeft: 10, marginTop: 20}}>
        <Text style={{fontWeight: 'bold', fontSize: 20, marginTop: 20}}>
          Message
        </Text>

        <TextInput
          style={{
            borderBottomWidth: 1,
            borderBottomColor: 'grey',
            paddingRight: 100,
          }}
          placeholder="Type Your Message Here"
          keyboardType="alphanumeric"
          multiline={true}
        />
      </View>
    </View>

    <View style={{alignItems: 'center'}}>
      <TouchableOpacity
        style={{
          // alignItems: 'center',
          borderWidth: 5,
          borderColor: '#06112d',
          borderRadius: 50,
          padding: 5,
          width: '50%',
          marginTop: 20,
          // marginLeft: 50,
          backgroundColor: '#06112d',
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: 'white',
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          SEND
        </Text>
      </TouchableOpacity>
    </View>
    <View style={{marginTop:190}}>
        <NewBottomDemo />
    </View>
  </SafeAreaView>
);

export default ContactDetails;
