import React from 'react';
import {createRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Linking,
  Image,
} from 'react-native';
import Filter from 'react-native-vector-icons/MaterialCommunityIcons';
import Pill from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../../Component/Header/Header';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  responsiveScreenHeight,
} from 'react-native-responsive-dimensions';
import {Alert} from 'react-native';
export default function Sos({navigation}) {
  console.log('JHello');
  const openDialScreen = () => {
    console.log('hello');
    let number = '';
    if (Platform.OS === 'ios') {
      number = 'telprompt:${8108372354}';
    } else {
      number = 'tel:${8108372354}';
    }
    Linking.openURL(number);
  };
  return (
    <SafeAreaView style={{}}>
      <Header navigation={navigation} />
      <ScrollView style={{backgroundColor: '#515151'}}>
        <View style={{height: responsiveHeight(100)}}>
          {/* <Text style={{padding:20,fontWeight:'bold',fontSize:responsiveFontSize(3)}}>SOS</Text> */}
          <View style={{marginBottom: 10}}>
            <TouchableOpacity
              style={{alignItems: 'center', elevation: 5}}
              onPress={() =>
                Alert.alert('“Save Our Souls”', 'Call Me : 8108372354', [
                  {
                    text: 'Call Now',
                    onPress: () => {
                      openDialScreen();
                    },
                  },
                  {
                    text: 'Message Now',
                    //  onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ])
              }>
              <Image
                style={{
                  height: responsiveHeight(40),
                  width: responsiveWidth(80),
                }}
                source={require('../../assets/sos.png')}
              />
            </TouchableOpacity>

            <Image
              source={require('../../assets/fingerpic.png')}
              style={{
                height: responsiveScreenHeight(20),
                width: responsiveWidth(40),
                alignSelf: 'center',
                resizeMode: 'center',
              }}
            />
          </View>

          <View
            style={{
              borderWidth: 2,
              width: responsiveWidth(52),
              height: responsiveHeight(15),
              alignSelf: 'center',
              marginBottom: 30,
              borderColor: 'white',
              justifyContent: 'center',
            }}>
            <Text style={{textAlign: 'center', color: 'white'}}>
              CLICK ON THE SOS IF YOU {'\n'}
              FEEL UNWEEL.WRITE A {'\n'}
              MESSAGE TO DISPLAY TO {'\n'}
              YOUR FRIENDS.
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

// import React from 'react';
// import {View, Text} from 'react-native';
// const Sos = () => {
//   return (
//     <View>
//       <Text>Hello</Text>
//     </View>
//   );
// };

//export default Sos;
