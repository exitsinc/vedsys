import ActionSheet from 'react-native-actions-sheet';
import React, {createRef, useEffect, useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  Modal,
  Image,
  StyleSheet,
  ActionSheetIOS,
  ViewPropTypes,
} from 'react-native';
//import Modal from 'react-native-modal';
import SignUp from '../../Component/SignUp';
import Login from '../../Component/Login';
import ForgotPassword from '../../Component/ForgotPassword';
//import ForgetPassword from '../../Component/ForgotPassword';
import QuestionOne from '../../Component/QuestionOne';
import QuestionTwo from '../../Component/QuestionTwo';
import QuestionThree from '../../Component/QuestionThree';
import QuestionFour from '../../Component/QuestionFour';
import QuestionFive from '../../Component/QuestionFive';
import QuestionSix from '../../Component/QuestionSix';
import Medication from '../../Component/Medication';
import SelectLanguage from '../../Component/SelectLanguage';
import Screen from '../lastscreen/Screen';
import AddRoutine from '../AddRoutine/AddRoutine';
import Dropdown from 'react-native-vector-icons/AntDesign';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
export default function HomeScreen({navigation, route}) {
  const [contact, showContact] = useState(false);
  const [sheet, showsheet] = useState(true);
  const [toggleActionSheet, setToggleActionSheet] = useState(2);
  const [modalSwitch, setModalSwitch] = useState(0);

  React.useEffect(() => {
    setModalSwitch(1);
  }, []);
  const Contact = () => {
    showContact(!contact);
    showsheet(!sheet);
    // navigation.navigate('Home');
    //setToggleActionSheet(2)
  };
  const showHome = () => {
    setModalSwitch(0);
    showsheet(true);
    setToggleActionSheet(0);
  };
  // let actionSheet;
  const actionSheetRef = createRef();

  const gotoSignUp = () => {
    setToggleActionSheet(1);
    console.log('ToggleData signup >>>>', toggleActionSheet);
    actionSheetRef.current?.setModalVisible();
  };

  const gotoLogin = () => {
    setToggleActionSheet(2);
    console.log('ToggleData login >>>>', toggleActionSheet);
    actionSheetRef.current?.setModalVisible();
  };

  const gotoLanguage = () => {
    setToggleActionSheet(9);
    actionSheetRef.current?.setModalVisible();
  };
  return (
    <>
      <>
        <ImageBackground
          source={require('../../Images/11.png')}
          style={{width: '100%', height: '100%'}}>
          {modalSwitch == 1 && (
            <Modal
              animationType="fade"
              transparent={true}
              visible={contact}
              onRequestClose={() => {
                // Alert.alert('Modal has been closed.');
                // showContact(!contact);
              }}>
              <View style={styles.found}>
                <Image
                  source={require('../../assets/agenda.png')}
                  style={{marginBottom: 20}}
                />
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.5),
                    marginBottom: 5,
                    color: '#003366',
                    fontWeight: 'bold',
                  }}>
                  Allow your contacts
                </Text>
                <View>
                  <Text style={{color: '#003366', textAlign: 'center'}}>
                    We Need these permissions to give
                    {'\n'}
                    you better user experience
                  </Text>
                </View>

                {/* <Text style={{marginBottom: 25, color: '#003366'}}>
               
              </Text> */}
                <TouchableOpacity
                  onPress={() => setModalSwitch(2)}
                  style={{
                    marginTop: 12,
                    justifyContent: 'center',
                    height: responsiveFontSize(7),
                  }}>
                  <Text style={styles.btn}>Sure I'd Like that</Text>
                </TouchableOpacity>
                <Text
                  // onPress={toggleModal}
                  style={{color: '#003366'}}>
                  Not Now
                </Text>
              </View>
            </Modal>
          )}
          {modalSwitch == 2 && (
            <Modal
              animationType="fade"
              transparent={true}
              visible={contact}
              onRequestClose={() => {
                // Alert.alert('Modal has been closed.');
                // showContact(!contact);
              }}>
              <View style={styles.found}>
                <Image
                  source={require('../../assets/camera.png')}
                  style={{marginBottom: 20}}
                />
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.5),
                    marginBottom: 5,
                    color: '#003366',
                    fontWeight: 'bold',
                  }}>
                  Allow Camera
                </Text>
                <View>
                  <Text style={{color: '#003366', textAlign: 'center'}}>
                    We Need these permissions to give
                    {'\n'}
                    you better user experience
                  </Text>
                </View>

                <TouchableOpacity
                  onPress={() => setModalSwitch(3)}
                  style={{
                    marginTop: 10,
                    justifyContent: 'center',
                    height: responsiveFontSize(7),
                  }}>
                  <Text style={styles.btn}>Sure I'd Like that</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text
                    // onPress={toggleModal}
                    style={{color: '#003366'}}>
                    Not Now
                  </Text>
                </TouchableOpacity>
              </View>
            </Modal>
          )}

          {modalSwitch == 3 && (
            <Modal
              animationType="fade"
              transparent={true}
              visible={contact}
              onRequestClose={() => {
                // Alert.alert('Modal has been closed.');
                // showContact(!contact);
              }}>
              <View style={styles.found}>
                <Image
                  source={require('../../assets/microphone.png')}
                  style={{marginBottom: 20}}
                />
                <Text
                  style={{
                    fontSize: responsiveFontSize(2.5),
                    marginBottom: 5,
                    color: '#003366',
                    fontWeight: 'bold',
                  }}>
                  Allow Microphone
                </Text>
                <View>
                  <Text style={{color: '#003366', textAlign: 'center'}}>
                    We Need these permissions to give
                    {'\n'}
                    you better user experience
                  </Text>
                </View>

                <TouchableOpacity
                  onPress={() => setModalSwitch(4)}
                  style={{
                    marginTop: 10,
                    justifyContent: 'center',
                    height: responsiveFontSize(7),
                  }}>
                  <Text style={styles.btn}>Sure I'd Like that</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text
                    // onPress={toggleModal}
                    style={{color: '#003366'}}>
                    Not Now
                  </Text>
                </TouchableOpacity>
              </View>
            </Modal>
          )}

          {modalSwitch == 4 && (
            <Modal
              animationType="fade"
              transparent={true}
              visible={contact}
              onRequestClose={() => {
                // Alert.alert('Modal has been closed.');
                // showContact(!contact);
              }}>
              <View style={styles.found}>
                <Image
                  source={require('../../assets/media.png')}
                  style={{marginBottom: 20}}
                />
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    marginBottom: 5,
                    color: '#003366',
                    fontWeight: 'bold',
                  }}>
                  Allow to store Files and Media
                </Text>
                <View>
                  <Text style={{color: '#003366', textAlign: 'center'}}>
                    We Need these permissions to give
                    {'\n'}
                    you better user experience
                  </Text>
                </View>

                <TouchableOpacity
                  onPress={showHome}
                  style={{
                    marginTop: 10,
                    justifyContent: 'center',
                    height: responsiveFontSize(7),
                  }}>
                  <Text style={styles.btn}>Sure I'd Like that</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text
                    // onPress={toggleModal}
                    style={{color: '#003366'}}>
                    Not Now
                  </Text>
                </TouchableOpacity>
              </View>
            </Modal>
          )}
          <View
            style={{
              //flex: 1,
              marginTop: 10,
              padding: 10,
              marginRight: 20,
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}>
            <TouchableOpacity
              style={{
                // backgroundColor: 'green',
                width: vw(32),
                height: vh(6),
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 50,
                borderWidth: 2,
                padding: 5,
                // paddingBottom: 10,
                borderColor: '#06112D',
              }}
              onPress={gotoLanguage}>
              <Image source={require('../../assets/language.png')} />
              <Text style={{fontSize: responsiveFontSize(2)}}> English </Text>
              <Dropdown name="caretdown" size={15} />
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 50,
              // marginLeft: 70,
              alignItems: 'center',
            }}>
            <View>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: responsiveFontSize(3),
                  fontWeight: 'bold',
                  width: '65%',
                }}>
                Say Hello to {'\n'}your's Shravan
                {/* {toggleActionSheet} */}
              </Text>
            </View>
          </View>
          <View
            style={{
              marginTop: 10,
              alignItems: 'center',
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 12,
                width: '78%',
                fontWeight: 'bold',
              }}>
              Now its easy to find out your fitness status{'\n'}and pill
              reminders on your hands
            </Text>
          </View>
          <View style={{flex: 1, justifyContent: 'flex-end', marginBottom: 20}}>
            <TouchableOpacity
              style={{
                backgroundColor: '#FFF',
                padding: 10,
                width: 200,
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 30,
                marginBottom: 15,
              }}
              onPress={gotoLogin}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 15,
                  fontWeight: '600',
                  textAlign: 'center',
                }}>
                Login
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor: '#062816',
                padding: 10,
                width: 200,
                justifyContent: 'center',
                alignSelf: 'center',
                borderRadius: 30,
              }}
              onPress={gotoSignUp}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 15,
                  fontWeight: '600',
                  textAlign: 'center',
                }}>
                Sign Up
              </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>

        {modalSwitch == 0 ||
          (sheet && toggleActionSheet && (
            <>
              <View
                style={{
                  justifyContent: 'center',
                  flex: 1,
                }}>
                <ActionSheet ref={actionSheetRef}>
                  {toggleActionSheet == 1 && (
                    <SignUp
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 2 && (
                    <Login
                      setToggleActionSheet={setToggleActionSheet}
                      navigation={navigation}
                      route={route}
                    />
                  )}
                  {toggleActionSheet == 3 && (
                    <QuestionOne
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 4 && (
                    <QuestionTwo
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 5 && (
                    <QuestionThree
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 6 && (
                    <QuestionFour
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 7 && (
                    <QuestionFive
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                  {toggleActionSheet == 8 && (
                    <QuestionSix
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                      navigation={navigation}
                      route={route}
                    />
                  )}
                  {toggleActionSheet == 14 && (
                    <Medication
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                      navigation={navigation}
                      route={route}
                    />
                  )}
                  {toggleActionSheet == 15 && (
                    <ForgotPassword
                      toggleState={toggleActionSheet}
                      setToggleActionSheet={setToggleActionSheet}
                      navigation={navigation}
                      route={route}
                    />
                  )}

                  {toggleActionSheet == 9 && (
                    <SelectLanguage
                      toggleState={toggleActionSheet}
                      contact={Contact}
                      setToggleActionSheet={setToggleActionSheet}
                    />
                  )}
                </ActionSheet>
              </View>
            </>
          ))}
      </>
    </>
  );
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#003366',
    // height:rH(5),
    borderWidth: 1,
    width: 138,
    borderRadius: 20,
    textAlign: 'center',
    // justifyContent: 'center',
    marginBottom: 18,
    padding: 7,
    color: 'white',

    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: '#FFFFF',
    // height: 300,
    // width: '85%',
    // borderRadius: 10,
    // borderWidth: 1,
    // borderColor: '#fff',
    // marginTop: 80,
    // marginLeft: 30,
    // elevation: 5,
  },
  found: {
    // height: vh(40),
    // width: vw(80),
    backgroundColor: 'white',
    borderRadius: 25,
    alignItems: 'center',
    //padding: '10%',
    justifyContent: 'center',
    // alignItems: 'center',
    //backgroundColor: '#FFFFF',
    height: 280,
    width: '75%',
    // borderRadius: 10,
    //borderWidth: 1,
    //  borderColor: '#fff',
    marginTop: 80,
    marginLeft: 50,
    elevation: 5,
  },
});
