import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import ProgressCircle from 'react-native-progress-circle';
import BackArrow from 'react-native-vector-icons/AntDesign';
import {startCounter, stopCounter} from 'react-native-accurate-step-counter';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import SelectDropdown from 'react-native-select-dropdown';
import Down from 'react-native-vector-icons/AntDesign';
import BarChart from 'react-native-bar-chart';
import {useDispatch, useSelector} from 'react-redux';
import {setStepCount} from '../../Redux/Slice/LoginSlice';
const Data = ['Last 7 days', 'Last Week', 'This Month', 'Last Month'];
const stepdata = [500, 700, 1200, 500, 1300, 1500, 900];

const horizontalData = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];
const StepCount = ({navigation}) => {
  const dispatch = useDispatch();
  const [steps, setSteps] = useState(0);
  useEffect(() => {
    const config = {
      default_threshold: 15.0,
      default_delay: 150000000,
      cheatInterval: 3000,
      onStepCountChange: stepCount => {
        setSteps(stepCount);
        dispatch(setStepCount(stepCount));
        // console.log("StepCount>>>",stepCount);
      },
      onCheat: () => {
        console.log('User is Cheating');
      },
    };
    startCounter(config);
    return () => {
      stopCounter();
    };
  }, []);

  return (
    <ImageBackground
      source={require('../../assets/Running.jpg')}
      style={{width: '100%', height: '100%'}}>
      <View
        style={{
          flexDirection: 'column',
          margin: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 8,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('Screen')}>
            <BackArrow name="arrowleft" size={25} />
          </TouchableOpacity>
          <SelectDropdown
            data={Data}
            defaultButtonText="Last 7 days"
            buttonTextStyle={{fontSize: vf(2), color: '#000'}}
            buttonStyle={{
              height: vh(6),
              width: vw(38),
              borderRadius: 10,
              backgroundColor: '#FF9E9D',
            }}
            dropdownStyle={{backgroundColor: '#F9CDAD', borderRadius: 10}}
            renderDropdownIcon={() => {
              return <Down name="down" size={20} color={'#000'} />;
            }}
            dropdownIconPosition={'right'}
            onSelect={(selectedItem, index) => {
              console.log(selectedItem, index);
            }}
            onSelect={(selectedItem, index) => {
              console.log(selectedItem, index);
              //  handleTextChange('Data', selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              // text represented after item is selected
              // if data array is an array of objects then return selectedItem.property to render after item is selected
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              // text represented for each item in dropdown
              // if data array is an array of objects then return item.property to represent item in dropdown
              return item;
            }}
          />
        </View>
        <View
          style={{
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ProgressCircle
            percent={50}
            radius={70}
            borderWidth={8}
            // color="33BEFF"
            color="#9cd161"
            // shadowColor="#999"
            bgColor="#fff">
            <Text style={{fontSize: 20}}>{steps}</Text>
            {/* <Icon name={item.iconname} size={45} color={item.iconcolor} /> */}
          </ProgressCircle>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            // margin: 20,
            // borderRadius: 50,
            // borderWidth: 1,
            // borderColor: '#FFF',
            // elevation: 2,
            padding: 10,
            // width: vw(80),
            // backgroundColor: '#fff',
            // marginTop: 80,
          }}>
          <View
            style={{
              flexDirection: 'column',
              backgroundColor: '#DBACB7',
              elevation: 2,
              height: vh(10),
              width: vw(25),
              padding: 5,
              borderRadius: 10,
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: 'bold'}}>Km</Text>
            <Text style={{fontWeight: 'bold'}}>1.3Km</Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              backgroundColor: '#DBACB7',
              elevation: 2,
              height: vh(10),
              width: vw(25),
              padding: 5,
              borderRadius: 10,
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: 'bold'}}>Cal</Text>
            <Text style={{fontWeight: 'bold'}}>130kCal</Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              backgroundColor: '#DBACB7',
              elevation: 2,
              height: vh(10),
              width: vw(25),
              padding: 5,
              borderRadius: 10,
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: 'bold'}}>Speed</Text>
            <Text style={{fontWeight: 'bold'}}>2.3km/hr</Text>
          </View>
        </View>
        <View style={{}}>
          <BarChart
            data={stepdata}
            barColor={'#A7DBD8'}
            horizontalData={horizontalData}
            //height={vh(38)}
            labelColor={'#000'}
            barLabelColor={'#e2e2e3'}
          />
        </View>
      </View>
    </ImageBackground>
  );
};

export default StepCount;
