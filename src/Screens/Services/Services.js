/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import Filter from 'react-native-vector-icons/MaterialCommunityIcons';
import Pill from 'react-native-vector-icons/MaterialCommunityIcons';
import Hand from 'react-native-vector-icons/FontAwesome5';
import Spoon from 'react-native-vector-icons/MaterialCommunityIcons';
import Grocery from 'react-native-vector-icons/MaterialIcons';
import Camera from '../Camera/Camera';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import ApiCalls from '../../Api/Api';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../../Component/Header/Header';
import NewBottomDemo from '../../Component/NewBottomDemo';
import {setCategories} from '../../Redux/Slice/LoginSlice';
import {
  setPharmacyService,
  setCareTakerService,
  setGroceryService,
  setFoodService,
  setDoctorService,
} from '../../Redux/Slice/ServicesSlice';
import {setloading} from '../../Redux/Slice/HomeSlice';
const call = new ApiCalls();
const Services = ({navigation}) => {
  const dispatch = useDispatch();
  let categories = [];
  let services = [];
  let servicesData = [];
  const categoryData = useSelector(reduxState => reduxState.login.categories);
  // console.log('Categories Data>>', categoryData);
  // console.log(
  //   'Categories Name >>>',
  //   categoryData.length > 0 && categoryData[0].name,
  // );
  React.useEffect(() => {
    // console.log('Hello From Services');
    (async () => {
      dispatch(setloading(true));
      // categories = await call.Calls('category', 'GET');
      // console.log('Categories Data>>>>', categories.data.results);
      services = await call.Calls('service', 'GET');
      console.log('Services Data>>>>:', services.data.results);
      //dispatch(setCategories(categories.data.results));

      servicesData = services.data.results;
      console.log('Services Data  For filter>>>', servicesData);

      let pharmacy = servicesData.filter((d, i) => {
        if (d.categories == '618e509d4d53c8214bfa3abe') {
          console.log('Pharmacy data>>', d);
          return d;
        }
      });
      let caretaker = servicesData.filter((d, i) => {
        if (d.categories == '618e50f34d53c8214bfa3ac1') {
          console.log('Care data>>', d);
          return d;
        }
      });

      let grocery = servicesData.filter((d, i) => {
        if (d.categories == '618e51c34d53c8214bfa3ac8') {
          console.log('grocery data>>', d);
          return d;
        }
      });

      let food = servicesData.filter((d, i) => {
        if (d.categories == '618e516a4d53c8214bfa3ac4') {
          console.log('Food data>>', d);
          return d;
        }
      });

      let doctor = servicesData.filter((d, i) => {
        if (d.categories == '618e4d684d53c8214bfa3aad') {
          console.log('Doctor data>>', d);
          return d;
        }
      });

      console.log('PharmaDATA >>', pharmacy);
      console.log('CareDATA >>', caretaker);
      console.log('Grocery Data >>', grocery);
      console.log('Food DATA >>', food);
      console.log('Doctor DATA >>', doctor);
      dispatch(setPharmacyService(pharmacy));
      dispatch(setCareTakerService(caretaker));
      dispatch(setGroceryService(grocery));
      dispatch(setFoodService(food));
      dispatch(setDoctorService(doctor));
      dispatch(setServices(services.data.results));
      dispatch(setloading(false));
    })();
  }, []);
  return (
    <SafeAreaView>
      <Header navigation={navigation} />
      <View>
        <View>
          <Text style={{fontSize: 20, fontWeight: 'bold', padding: 20}}>
            Services
          </Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity onPress={() => navigation.navigate('FindDoctor')}>
            <View
              style={{
                backgroundColor: '#d6ccfe',
                padding: 16,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Filter name="stethoscope" size={32} color="#5226fb" />
              <Text
                style={{
                  color: `${categoryData ? categoryData[0].color : null}`,
                  marginTop: 5,
                }}>
                {categoryData[0].name}
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('FindMedicine')}>
            <View
              style={{
                backgroundColor: '#ceefff',
                padding: 10,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Pill name="pill" size={32} color="#2cb9fd" />
              <Text
                style={{
                  color: `${categoryData ? categoryData[1].color : null}`,
                  marginTop: 10,
                }}>
                {categoryData[1].name}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => navigation.navigate('CareTacker')}>
            <View
              style={{
                backgroundColor: '#dff0cc',
                padding: 16,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Hand name="hand-holding-heart" size={32} color="#abd879" />
              <Text
                style={{
                  color: `${categoryData ? categoryData[2].color : null}`,
                  marginTop: 10,
                }}>
                {categoryData[2].name}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{marginTop: 20}} />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-start',
            paddingLeft: 10,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('FoodServices')}>
            <View
              style={{
                backgroundColor: '#fef4d7',
                padding: 16,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Filter name="silverware-fork-knife" size={32} color="#fcc836" />
              <Text
                style={{
                  color: `${categoryData ? categoryData[3].color : null}`,
                  marginTop: 10,
                }}>
                {categoryData[3].name}
              </Text>
            </View>
          </TouchableOpacity>
          <View style={{paddingLeft: 15}} />
          <TouchableOpacity onPress={() => navigation.navigate('Grocery')}>
            <View
              style={{
                backgroundColor: '#ffbbbc',
                padding: 16,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Grocery name="local-grocery-store" size={32} color="#ff696a" />
              <Text
                style={{
                  color: `${categoryData ? categoryData[4].color : null}`,
                  marginTop: 10,
                }}>
                {categoryData[4].name}
              </Text>
            </View>
          </TouchableOpacity>
          {/* <CustomButton
        title="open camera"
        color="#0080ff"
        onPressFunction={() => {
          navigation.navigate('Camera');
        }}
      /> */}
          {/* <TouchableOpacity onPress={() => navigation.navigate('Camera')}>
            <View
              style={{
                backgroundColor: '#ffbbbc',
                padding: 16,
                borderRadius: 15,
                alignItems: 'center',
                height: 100,
                width: 105,
              }}>
              <Grocery name="local-grocery-store" size={32} color="#ff696a" />
              <Text style={{color: '#ff696a', marginTop: 10}}>Open Camera</Text>
            </View>
          </TouchableOpacity> */}
        </View>
      </View>
      {/* <View style={{marginTop: 220}}>
        <NewBottomDemo />
      </View> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default Services;
