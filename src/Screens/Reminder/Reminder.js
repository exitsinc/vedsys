import React, {createRef, useState} from 'react';
import CalendarStrip from 'react-native-calendar-strip';
import Filter from 'react-native-vector-icons/MaterialCommunityIcons';
import Pill from 'react-native-vector-icons/MaterialCommunityIcons';
import Break from 'react-native-vector-icons/MaterialIcons';
import Fi from 'react-native-vector-icons/MaterialIcons';
import ActionSheet from 'react-native-actions-sheet';
import MorePrescription from '../../Component/MorePrescription/MorePrescription';
import PrescribeName from '../../Component/PrescribeName/PrescribeName';
import {useSelector} from 'react-redux';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Header from '../../Component/Header/Header';

const morning = [
  {
    id: 1,
    src: require('../../assets/images/oxycodone1.png'),
    heading: 'Oxycodone',
    pillsLeft: '20 Pills Left',
    subheading: 'Before Breakfast',
    time: '6-7am Approx',
    no_pills: '1 Pill',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 2,
    src: require('../../assets/images/medicine.png'),
    heading: 'B 12',
    pillsLeft: '20 Pills Left',
    subheading: 'After Breakfast',
    time: '8-9am Approx',
    no_pills: '1 Pill',
  },
  {
    id: 3,
    src: require('../../assets/images/vitamin.png'),
    heading: 'Vitamin C',
    pillsLeft: '20 Pills Left',
    subheading: 'After Breakfast',
    time: '8-9am Approx',
    no_pills: '1 Pill',
  },
];

const afternoon = [
  {
    id: 1,
    src: require('../../assets/images/medicine.png'),
    heading: 'B 12',
    pillsLeft: '20 Pills Left',
    subheading: 'After Lunch',
    time: '8-9am Approx',
    no_pills: '1 Pill',
  },
  {
    id: 2,
    src: require('../../assets/images/vitamin.png'),
    heading: 'Vitamin C',
    pillsLeft: '20 Pills Left',
    subheading: 'After Lunch',
    time: '2-3pm Approx',
    no_pills: '1 Pill',
  },
];

const night = [
  {
    id: 1,
    src: require('../../assets/images/medicine.png'),
    heading: 'B 12',
    pillsLeft: '20 Pills Left',
    subheading: 'Before Bed',
    time: '8-9pm Approx',
    no_pills: '1 Pill',
  },
  {
    id: 2,
    src: require('../../assets/images/vitamin.png'),
    heading: 'Vitamin C',
    pillsLeft: '20 Pills Left',
    subheading: 'Before Bed',
    time: '8-9pm Approx',
    no_pills: '1 Pill',
  },
];
export default function Reminder({navigation, route}) {
  const actionSheetRef = createRef();
  const [toggleActionSheet, setToggleActionSheet] = useState();
  const gotoPrescription = () => {
    setToggleActionSheet(11);
    actionSheetRef.current?.setModalVisible();
  };

  const data = useSelector(reduxState => reduxState.login.medicineDetails);
  console.log('MedicineDetails>>', data);
  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={{backgroundColor: 'white'}}>
          <View style={{width: '100%', backgroundColor: 'white'}}>
            <Header navigation={navigation} />
            <View style={styles.head}>
              <View>
                <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
                  Reminder
                </Text>
              </View>
              <View>
                <TouchableOpacity onPress={gotoPrescription}>
                  <Fi name="filter-alt" size={25} color="dark-black" />
                </TouchableOpacity>
              </View>
            </View>
            <View>
              <View style={styles.calendar}>
                <CalendarStrip
                  style={{height: 80}}
                  dateNumberStyle={{color: 'black', borderRadius: 5}}
                  highlightDateNumberStyle={{
                    // color: 'white',
                    padding: 20,
                    borderRadius: 50,
                    backgroundColor: '#7daa2f',
                    color: '#fff',
                  }}
                />
              </View>
            </View>
            <View style={{marginTop: -10}}>
              <Text style={styles.weekdays}>Morning</Text>
            </View>
            <FlatList
              style={{margin: 15}}
              data={data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    margin: 0,
                    marginTop: 8,
                    // padding: 10,
                    height: vh(12),
                    borderColor: '#D5DBDB',
                    borderWidth: 1,
                    borderRadius: 10,
                    //justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      height: vh(12),
                      borderRadius: 10,
                      width: vw(22),
                    }}>
                    <Image
                      // source={item.src}
                      style={{
                        height: vh(12),
                        width: vw(22),
                        borderTopLeftRadius: 10,
                        borderBottomLeftRadius: 10,
                      }}
                      source={require('../../assets/images/oxycodone1.png')}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'column',
                      marginLeft: 5,
                      padding: 5,
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
                        {item.medicineName}
                      </Text>
                      <Text
                        style={{
                          fontSize: vf(1.5),
                          color: '#9CD161',
                          marginRight: 5,
                        }}>
                        {/* {item.pillsLeft} */}20 Pills Left
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: -10,
                        }}>
                        <Break name="free-breakfast" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>
                          {/* {item.subheading} */}Before Breakfast
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 3,
                        }}>
                        <Filter name="clock-outline" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>
                          {/* {item.time} */}
                          6-7 am Approx
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 2,
                        }}>
                        <Pill name="pill" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>
                          {/* {item.no_pills} */}1 Pill
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />

            <View>
              <Text style={styles.weekdays}>AfterNoon</Text>
            </View>
            <FlatList
              style={{margin: 15}}
              data={afternoon}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    margin: 0,
                    marginTop: 8,
                    // padding: 10,
                    height: vh(12),
                    borderColor: '#D5DBDB',
                    borderWidth: 1,
                    borderRadius: 10,
                    //justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      height: vh(12),
                      borderRadius: 10,
                      width: vw(22),
                    }}>
                    <Image
                      source={item.src}
                      style={{
                        height: vh(12),
                        width: vw(22),
                        borderTopLeftRadius: 10,
                        borderBottomLeftRadius: 10,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'column',
                      marginLeft: 5,
                      padding: 5,
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
                        {item.heading}
                      </Text>
                      <Text
                        style={{
                          fontSize: vf(1.5),
                          color: '#9CD161',
                          //marginRight: 5,
                        }}>
                        {item.pillsLeft}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          //  marginLeft: -10,
                        }}>
                        <Break name="free-breakfast" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>
                          {item.subheading}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 5,
                        }}>
                        <Filter name="clock-outline" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>{item.time}</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 5,
                        }}>
                        <Pill name="pill" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>{item.no_pills}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />

            <View>
              <Text style={styles.weekdays}>Night</Text>
            </View>
            <FlatList
              style={{margin: 15}}
              data={night}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    margin: 0,
                    marginTop: 8,
                    // padding: 10,
                    height: vh(12),
                    borderColor: '#D5DBDB',
                    borderWidth: 1,
                    borderRadius: 10,
                    //justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      height: vh(12),
                      borderRadius: 10,
                      width: vw(22),
                    }}>
                    <Image
                      source={item.src}
                      style={{
                        height: vh(12),
                        width: vw(22),
                        borderTopLeftRadius: 10,
                        borderBottomLeftRadius: 10,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'column',
                      marginLeft: 5,
                      padding: 5,
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
                        {item.heading}
                      </Text>
                      <Text
                        style={{
                          fontSize: vf(1.5),
                          color: '#9CD161',
                          // marginRight: 5,
                        }}>
                        {item.pillsLeft}
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          // marginLeft: -10,
                        }}>
                        <Break name="free-breakfast" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>
                          {item.subheading}
                        </Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 5,
                        }}>
                        <Filter name="clock-outline" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>{item.time}</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          backgroundColor: '#F3F6F6',
                          borderRadius: 4,
                          marginLeft: 5,
                        }}>
                        <Pill name="pill" size={15} color="black" />
                        <Text style={{fontSize: vf(1.5)}}>{item.no_pills}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
            />
          </View>
        </View>
      </ScrollView>
      <ActionSheet ref={actionSheetRef}>
        {toggleActionSheet == 11 && (
          <MorePrescription
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
        {toggleActionSheet == 12 && (
          <PrescribeName
            toggleState={toggleActionSheet}
            setToggleActionSheet={setToggleActionSheet}
            navigation={navigation}
            route={route}
          />
        )}
      </ActionSheet>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  head: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 18,
    marginTop: -10,
  },
  calendar: {
    backgroundColor: 'white',
    padding: 20,
    marginTop: -10,
  },
  weekdays: {
    paddingLeft: 18,
    fontSize: vf(2.5),
    fontWeight: 'bold',
    //marginBottom: 10,
  },
});
