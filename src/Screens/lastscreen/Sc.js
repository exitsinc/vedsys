import React, {createRef, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ProgressCircle from 'react-native-progress-circle';
import {Item} from 'react-native-paper/lib/typescript/components/List/List';
import {useDispatch, useSelector} from 'react-redux';
import {resetMedicineDetails} from '../../Redux/Slice/LoginSlice';
const FootTab = [
  {
    activity: 'Calories',
    // src1: require('../../Images/walk-1.png'),
    iconname: 'fire',
    color: '#33BEFF',
    iconcolor: '#e25822',
    values: 'Calories Burnt',
  },
  {
    activity: 'Steps',
    // src1: require('../../Images/sleep-1.png'),
    iconname: 'walking',
    color: '#F51C0B',
    iconcolor: 'orange',
    values: 'Steps',
  },
  {
    activity: 'Next Visit',
    // src1: require('../../Images/Sports.png'),
    iconname: 'user-clock',
    color: '#9cd161',
    iconcolor: '#95cded',
    values: 'Pending Days',
  },
  {
    activity: 'Medicines',
    // src1: require('../../Images/Work.png'),
    iconname: 'capsules',
    color: '#000',
    iconcolor: '#9cd161',
    values: 'Medicines Left',
  },
];
function Sc(props) {
  const dispatch = useDispatch();
  const actionSheetRef = createRef();
  const {handleActionSheet1, navigation} = props;
  const clickHandler = item => {
    if (item.activity == 'Medicines') {
      // dispatch(resetMedicineDetails());
      handleActionSheet1();
    } else if (item.activity == 'Steps') {
      navigation.replace('StepCount');
      console.log('steps click done');
    }
  };
  return (
    <View>
      <FlatList
        data={FootTab}
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'space-evenly',
          marginBottom: 10,
        }}
        renderItem={({item}) => (
          <View style={styles.container}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: 'bold',
                fontSize: responsiveFontSize(2.5),
              }}>
              {item.activity}
            </Text>
            <View
              style={{
                marginTop: 15,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {/* <Image
                style={{
                  height: responsiveHeight(15.16),
                  width: responsiveWidth(27.16),
                  borderWidth: 6.5,
                  borderColor: item.color,
                  borderRadius: 80,
                  alignSelf: 'center',
                }}
                resizeMode="cover"
                source={item.src1}
              /> */}
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                }}
                onPress={() => clickHandler(item)}
                //</View> onPress={() => navigation.navigate('StepCount')}
              >
                <ProgressCircle
                  percent={30}
                  radius={50}
                  borderWidth={8}
                  color={item.color}
                  shadowColor="#999"
                  bgColor="#fff">
                  <Icon name={item.iconname} size={45} color={item.iconcolor} />
                </ProgressCircle>
                <Text style={{fontSize: responsiveFontSize(1.8)}}>
                  No. Of {item.values}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    // height: responsiveHeight(24.5),
    height: responsiveHeight(26.5),
    width: responsiveWidth(45),
    marginBottom: 5,
    padding: 1,

    borderWidth: 1,
    borderColor: '#f5f5f5',
    borderRadius: 10,
    elevation: 1,
  },
});
export default Sc;
