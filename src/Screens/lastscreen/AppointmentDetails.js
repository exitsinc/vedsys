import React from 'react';
import {
  View,
  Text,
  SectionList,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
import {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
const AppointmentDetails = () => {
  const data = useSelector(reduxState => reduxState.login.appointment);
  console.log('Appontment Details??>>>>', data);
  return (
    <>
      <View
        style={{height: rh(85), padding: 15, justifyContent: 'space-evenly'}}>
        <Text style={{fontSize: rf(2.9), fontWeight: 'bold'}}>
          Appointment Details
        </Text>
        <Text style={styles.textdesign}>Hospital Name:{data.hospitalName}</Text>
        <Text style={styles.textdesign}>Doctor Name:{data.doctorName}</Text>
        <Text style={styles.textdesign}>Date:{data.appointmentDate}</Text>
        <Text style={styles.textdesign}>Time:{data.appointmentTime}</Text>
        <Text style={styles.textdesign}>Symptoms:{data.symptoms}</Text>
        <Text style={styles.textdesign}>Department Name:{data.department}</Text>
      </View>
    </>
  );
};

export default AppointmentDetails;

const styles = StyleSheet.create({
  // container: {
  //  flex: 1,
  //  paddingTop: 22
  // },
  textdesign: {
    fontSize: rf(2.2),
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 18,
    // width: '100%',
    fontWeight: 'bold',
    backgroundColor: 'rgba(235,247,247,1.0)',
    //backgroundColor: '#BEBFC5',
  },
  item: {
    padding: 10,
    fontSize: 16,
    height: 44,
  },
});
