import React, {useState, useEffect, createRef} from 'react';
import {
  StyleSheet,
  Text,
  Image,
  ScrollView,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
import ActionSheet from 'react-native-actions-sheet';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SelectDropdown from 'react-native-select-dropdown';
import DateTimePicker from '@react-native-community/datetimepicker';
import {useDispatch, useSelector} from 'react-redux';
import {setMedicineDetails} from '../../Redux/Slice/LoginSlice';
import Accordion from 'react-native-collapsible/Accordion';
import MedicineDetails from './MedicineDetails';
//const time = ['Morning', 'AfterNoon', 'Night'];

const SECTIONS = [
  {
    day: 'monday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
  {
    day: 'tuesday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
  {
    day: 'wednesday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
  {
    day: 'thursday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
];

const SECTIONS1 = [
  {
    day: 'friday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
  {
    day: 'saturday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
  {
    day: 'sunday',
    timing: ['Morning', 'AfterNoon', 'Night'],
  },
];

// const Instruction =[
//   {
//     id:1,
//     value:'Before Breakfast',
//     checked:false,
//   },
//   {
//     id:2,
//     value:'After Breakfast',
//     checked:false,
//   },
//   {
//     id:3,
//     value:'Before Lunch',
//     checked:false,
//   },
//   {
//     id:4,
//     value:'After Lunch',
//     checked:false,
//   },
//   {
//     id:5,
//     value:'Before Dinner',
//     checked:false,
//   },
//   {
//     id:6,
//     value:'After Dinner',
//     checked:false,
//   },
// ];
const AddMedicine = ({navigation, route}) => {
  const actionSheetRef = createRef();
  const [toggleActionSheet, setToggleActionSheet] = useState();
  const [toggle, showToggle] = useState(false);
  const [checkdata, setCheckData] = useState();
  const [toggleCheckBox1, setToggleCheckBox1] = useState(false);
  const [toggleCheckBox2, setToggleCheckBox2] = useState(false);
  const [toggleCheckBox3, setToggleCheckBox3] = useState(false);
  const [toggleCheckBox4, setToggleCheckBox4] = useState(false);
  const [toggleCheckBox5, setToggleCheckBox5] = useState(false);
  const [toggleCheckBox6, setToggleCheckBox6] = useState(false);
  // const [activeSections, setActiveSections] = useState([]);
  // const [activeSections1, setActiveSections1] = useState([]);
  // const setSections = sections => {
  //   setActiveSections(sections.includes(undefined) ? [] : sections);
  // };
  // const setSections1 = sections => {
  //   setActiveSections1(sections.includes(undefined) ? [] : sections);
  // };
  const dispatch = useDispatch();
  const [state, setState] = useState({
    medicineName: null,
    medicineMG: null,
    days: null,
    perDay: null,
    doctorName: null,
    startDate: null,
    endDate: null,
    // beforeBreakFast: false,
    // afterBreakFast: false,
    // beforeLunch: false,
    // afterLunch: false,
    // beforeNight: false,
    // afterNight: false,
    expireDate: null,
    medicalName: null,
    medicalAddress: null,
    mobileNo: null,
    email: null,
    upiId: null,
    bankAccount: null,
  });
  console.log('localstate >>>', state);

  const handleTextChange = (lbl, txt) => {
    console.log('txt', lbl, txt);
    setState({...state, [lbl]: txt});
  };
  const handleChange = (key, value) => {
    console.log('key,value>>>', key, value);
    setState({...state, [key]: value});
    // setState({
    //   ...state,
    //   timing: {
    //     ...state.timing,
    //     [day]: {...state.timing[day], [key]: value},
    //   },
    // });

    // setState({...state, [day.key]: false});
  };
  const gotoMedicineDetails = (lbl, txt) => {
    console.log('Click on Submit');
    dispatch(setMedicineDetails({...state, [lbl]: txt}));
    console.log('MedicineDispatch:::>>', {...state, [lbl]: txt});
    //setToggleActionSheet(16);
    //actionSheetRef.current?.setModalVisible();
  };

  useEffect(() => {
    console.log('USE EFFECT Medicine Details', state);
    setState({
      medicineName: null,
      medicineMG: null,
      days: null,
      perDay: null,
      doctorName: null,
      startDate: null,
      endDate: null,
      expireDate: null,
      medicalName: null,
      medicalAddress: null,
      mobileNo: null,
      email: null,
      upiId: null,
      bankAccount: null,
    });
  }, []);

  // const renderHeader = section => {
  //   return (
  //     <View
  //       style={{
  //         height: rh(8),
  //         width: rw(40),
  //         backgroundColor: '#EFEFEF',
  //         marginTop: 10,
  //         padding: 10,
  //         flexDirection: 'row',
  //         alignItems: 'center',
  //         textAlign: 'center',
  //         justifyContent: 'space-between',
  //         //  borderRadius: 10,
  //       }}>
  //       <Text>{section.day}</Text>
  //       <Icon name="arrow-drop-down" size={30} />
  //     </View>
  //   );
  // };
  // const renderHeader1 = section => {
  //   return (
  //     <View
  //       style={{
  //         height: rh(8),
  //         width: rw(40),
  //         backgroundColor: '#EFEFEF',
  //         marginTop: 10,
  //         flexDirection: 'row',
  //         alignItems: 'center',
  //         textAlign: 'center',
  //         padding: 10,
  //         justifyContent: 'space-between',
  //         //  borderRadius: 10,
  //       }}>
  //       <Text>{section.day}</Text>
  //       <Icon name="arrow-drop-down" size={30} />
  //     </View>
  //   );
  // };

  // const renderContent = section => {
  //   return (
  //     <View style={{margin: 5}}>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox1}
  //           onValueChange={data => {
  //             console.log('data>>', data);

  //             toggleCheckBox1
  //               ? setToggleCheckBox1(false)
  //               : setToggleCheckBox1(true);
  //             //  handleChange(section.day, 'morning', data);
  //           }}
  //           // onChange={() => {
  //           //   handleChange();
  //           // }}
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[0]}</Text>
  //       </View>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox2}
  //           onValueChange={() =>
  //             toggleCheckBox2
  //               ? setToggleCheckBox2(false)
  //               : setToggleCheckBox2(true)
  //           }
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[1]}</Text>
  //       </View>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox3}
  //           onValueChange={() =>
  //             toggleCheckBox3
  //               ? setToggleCheckBox3(false)
  //               : setToggleCheckBox3(true)
  //           }
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[2]}</Text>
  //       </View>
  //       {/* {SECTIONS.map((d, i) => {
  //         console.log('d>>>>>', d);
  //         return (
  //           <>
  //             <View style={{flexDirection: 'row'}}>
  //               <CheckBox
  //                 disabled={false}
  //                 value={toggleCheckBox}
  //                 onValueChange={() =>
  //                   toggleCheckBox
  //                     ? setToggleCheckBox(false)
  //                     : setToggleCheckBox(true)
  //                 }
  //               />
  //               <Text style={{marginTop: 5}}>{d.timing[i]}</Text>
  //             </View>
  //           </>
  //         );
  //       })} */}
  //     </View>
  //   );
  // };

  // const renderContent1 = section => {
  //   return (
  //     <View style={{margin: 5}}>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox1}
  //           onValueChange={data => {
  //             console.log('data>>', data);

  //             toggleCheckBox1
  //               ? setToggleCheckBox1(false)
  //               : setToggleCheckBox1(true);
  //             //  handleChange(section.day, 'morning', data);
  //           }}
  //           // onChange={() => {
  //           //   handleChange();
  //           // }}
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[0]}</Text>
  //       </View>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox2}
  //           onValueChange={() =>
  //             toggleCheckBox2
  //               ? setToggleCheckBox2(false)
  //               : setToggleCheckBox2(true)
  //           }
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[1]}</Text>
  //       </View>
  //       <View style={{flexDirection: 'row'}}>
  //         <CheckBox
  //           disabled={false}
  //           value={toggleCheckBox3}
  //           onValueChange={() =>
  //             toggleCheckBox3
  //               ? setToggleCheckBox3(false)
  //               : setToggleCheckBox3(true)
  //           }
  //         />
  //         <Text style={{marginTop: 5}}>{section.timing[2]}</Text>
  //       </View>
  //       {/* {SECTIONS.map((d, i) => {
  //         console.log('d>>>>>', d);
  //         return (
  //           <>
  //             <View style={{flexDirection: 'row'}}>
  //               <CheckBox
  //                 disabled={false}
  //                 value={toggleCheckBox}
  //                 onValueChange={() =>
  //                   toggleCheckBox
  //                     ? setToggleCheckBox(false)
  //                     : setToggleCheckBox(true)
  //                 }
  //               />
  //               <Text style={{marginTop: 5}}>{d.timing[i]}</Text>
  //             </View>
  //           </>
  //         );
  //       })} */}
  //     </View>
  //   );
  // };
  return (
    <View style={{height: rh(70)}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'column',
            padding: 10,
          }}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity>
              <View
                style={{
                  margin: 10,
                  borderWidth: 1,
                  borderColor: 'grey',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderStyle: 'dashed',
                  borderRadius: 10,
                  height: rh(25),
                  width: rw(85),
                }}>
                <Image source={require('../../assets/file-upload.png')} />
                <Text style={{fontSize: rf(2), marginTop: 25}}>
                  Upload Your Medicine Image
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{margin: 10, marginLeft: 25}}>
            {/* <Text style={{fontSize: rf(2.5)}}>Medicine Name</Text> */}
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.medicineName}
              onChangeText={e => {
                handleTextChange('medicineName', e);
              }}
              placeholder="Enter Medicine Name"
            />

            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9BA0AB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.medicineMG}
              onChangeText={e => {
                handleTextChange('medicineMG', e);
              }}
              placeholder="Enter Medicine MG"
            />
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextInput
                style={{
                  color: '#9BA0AB',
                  borderBottomColor: '#9BA0AB',
                  borderBottomWidth: 1,
                  fontSize: rf(1.8),
                  width: rw(40),
                  // onFocus={showDatepicker}
                }}
                value={state.days}
                onChangeText={e => {
                  handleTextChange('days', e);
                }}
                placeholder="Enter Days"
              />
              {/* <Text style={{fontSize: rf(1.8.5)}}>End Date</Text> */}
              <TextInput
                style={{
                  color: '#9BA0AB',
                  borderBottomColor: '#9BA0AB',
                  borderBottomWidth: 1,
                  fontSize: rf(1.8),
                  width: rw(40),
                  //onChangeText={showDatepicker}
                }}
                value={state.perDay}
                onChangeText={e => {
                  handleTextChange('perDay', e);
                }}
                placeholder=" Per Day Dosage"
              />
            </View>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TextInput
                style={{
                  color: '#9BA0AB',
                  borderBottomColor: '#9BA0AB',
                  borderBottomWidth: 1,
                  fontSize: rf(1.8),
                  width: rw(40),
                  // onFocus={showDatepicker}
                }}
                value={state.startDate}
                onChangeText={e => {
                  handleTextChange('startDate', e);
                }}
                placeholder="Start Date"
              />
              {/* <Text style={{fontSize: rf(1.8.5)}}>End Date</Text> */}
              <TextInput
                style={{
                  color: '#9BA0AB',
                  borderBottomColor: '#9BA0AB',
                  borderBottomWidth: 1,
                  fontSize: rf(1.8),
                  width: rw(40),
                  //onChangeText={showDatepicker}
                }}
                value={state.endDate}
                onChangeText={e => {
                  handleTextChange('endDate', e);
                }}
                placeholder="End Date"
              />

              {/* {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )} */}
            </View>
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9BA0AB',
                borderBottomWidth: 1,
                marginBottom: 10,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.doctorName}
              onChangeText={e => {
                handleTextChange('doctorName', e);
              }}
              placeholder="Enter Doctor Name"
            />
            <Text style={{fontSize: rf(2.2)}}>Medicine Schedule</Text>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'space-between',
              }}>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox1}
                    onValueChange={data => {
                      console.log('BeforeBreak-Data>>', data);
                      handleChange('beforeBreakFast', data);
                      toggleCheckBox1
                        ? setToggleCheckBox1(false)
                        : setToggleCheckBox1(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>Before Breakfast</Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox2}
                    onValueChange={data => {
                      console.log('AfterBreak-Data>>', data);
                      handleChange('afterBreakFast', data);
                      toggleCheckBox2
                        ? setToggleCheckBox2(false)
                        : setToggleCheckBox2(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>After Breakfast</Text>
                </View>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox3}
                    onValueChange={data => {
                      console.log('BeforeLunch-Data>>', data);
                      handleChange('beforeLunch', data);
                      toggleCheckBox3
                        ? setToggleCheckBox3(false)
                        : setToggleCheckBox3(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>Before Lunch</Text>
                </View>
                <View style={{flexDirection: 'row', marginRight: 25}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox4}
                    onValueChange={data => {
                      console.log('AfterLunch-Data>>', data);
                      handleChange('afterLunch', data);
                      toggleCheckBox4
                        ? setToggleCheckBox4(false)
                        : setToggleCheckBox4(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>After Lunch</Text>
                </View>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox5}
                    onValueChange={data => {
                      console.log('beforeDinner-Data>>', data);
                      handleChange('beforeDinner', data);
                      toggleCheckBox5
                        ? setToggleCheckBox5(false)
                        : setToggleCheckBox5(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>Before Dinner</Text>
                </View>
                <View style={{flexDirection: 'row', marginRight: 22}}>
                  <CheckBox
                    disabled={false}
                    value={toggleCheckBox6}
                    onValueChange={data => {
                      console.log('AfterDinner-Data>>', data);
                      handleChange('afterDinner', data);
                      toggleCheckBox6
                        ? setToggleCheckBox6(false)
                        : setToggleCheckBox6(true);
                    }}
                  />
                  <Text style={{marginTop: 5}}>After Dinner</Text>
                </View>
              </View>
            </View>
          </View>

          {/* <View
            style={{
              margin: 10,
              marginLeft: 25,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <Accordion
                activeSections={activeSections}
                sections={SECTIONS}
                touchableComponent={TouchableOpacity}
                renderHeader={renderHeader}
                renderContent={renderContent}
                duration={400}
                onChange={setSections}
                style={{
                  borderWidth: 1,
                  //borderRadius: 10,
                  borderColor: '#EFEFEF',
                }}
              />
              <Accordion
                activeSections={activeSections1}
                sections={SECTIONS1}
                touchableComponent={TouchableOpacity}
                renderHeader={renderHeader1}
                renderContent={renderContent1}
                duration={400}
                onChange={setSections1}
                style={{
                  borderWidth: 1,
                  //borderRadius: 10,
                  borderColor: '#EFEFEF',
                }}
              />
            </View>
          </View> */}

          <View style={{margin: 10, marginLeft: 20}}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: '#EFEFEF',
                justifyContent: 'space-between',
                borderRadius: 10,
                height: rh(8),
                padding: 10,
              }}
              onPress={() => showToggle(!toggle)}>
              <Text style={{fontSize: rf(2)}}>Optional</Text>
              <Icon name="arrow-drop-down" size={30} />
            </TouchableOpacity>
            {toggle ? (
              <View>
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.expireDate}
                  onChangeText={e => {
                    handleTextChange('expireDate', e);
                  }}
                  placeholder="Enter Expire Date"
                />
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.medicalName}
                  onChangeText={e => {
                    handleTextChange('medicalName', e);
                  }}
                  placeholder="Enter Medical Name"
                />

                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.medicalAddress}
                  onChangeText={e => {
                    handleTextChange('medicalAddress', e);
                  }}
                  placeholder="Enter Medical Address"
                />
                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.mobileNo}
                  onChangeText={e => {
                    handleTextChange('mobileNo', e);
                  }}
                  placeholder="Enter Mobile No"
                />

                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.email}
                  onChangeText={e => {
                    handleTextChange('email', e);
                  }}
                  placeholder="Enter Email"
                />

                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.upiId}
                  onChangeText={e => {
                    handleTextChange('upiId', e);
                  }}
                  placeholder="Enter UPI ID"
                />

                <TextInput
                  style={{
                    color: '#9BA0AB',
                    borderBottomColor: '#9BA0AB',
                    borderBottomWidth: 1,
                    marginBottom: 10,
                    fontSize: rf(1.8),
                    width: rw(85),
                  }}
                  value={state.bankAccount}
                  onChangeText={e => {
                    handleTextChange('bankAccount', e);
                  }}
                  placeholder="Enter Bank Account No"
                />
              </View>
            ) : null}
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <TouchableOpacity
              onPress={() => {
                gotoMedicineDetails();
              }}
              style={{
                backgroundColor: 'black',
                padding: 5,
                width: rw(30),
                borderRadius: 30,
                textAlign: 'center',
                height: rh(7),
                justifyContent: 'center',
                alignSelf: 'center',
                // marginBottom: 10,
              }}>
              <Text
                style={{
                  fontSize: rf(2),
                  color: 'white',
                  textAlign: 'center',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <ActionSheet ref={actionSheetRef}>
          {toggleActionSheet == 16 && (
            <MedicineDetails
              toggleState={toggleActionSheet}
              setToggleActionSheet={setToggleActionSheet}
              navigation={navigation}
              route={route}
            />
          )}
        </ActionSheet>
      </ScrollView>
    </View>
  );
};

export default AddMedicine;
