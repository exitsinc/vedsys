import React, {createRef, useState} from 'react';
import ActionSheet from 'react-native-actions-sheet';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import MedicineView from './MedicineView';
function Medicine(props) {
  // const [toggleActionSheet, setToggleActionSheet] = useState(false);
  //const {handleActionSheet} = props;
  const data = useSelector(reduxState => reduxState.login.medicineDetails);
  console.log('MedicineDetails>>', data);
  const [selectedItem, setSelectedItem] = useState({});
  const actionSheetRef = createRef();
  const handleActionSheet = () => {
    actionSheetRef.current?.setModalVisible();
  };
  const selectHandler = item => {
    console.log('item >> ', item);
    handleActionSheet();
    setSelectedItem(item);
  };
  //  if(data.beforeBreakFast === true ||data.afterBreakFast === true || data.beforeLunch===true || data.afterLunch === true || data.beforeDinner === true || data.afterDinner === true)
  //  {
  //    console.log()
  //  }
  return (
    <View>
      <FlatList
        style={{margin: 5}}
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <View style={styles.container}>
            <TouchableOpacity
              style={{flexDirection: 'row'}}
              onPress={() => selectHandler(item)}>
              <View style={styles.imgcont}>
                <Image
                  style={{
                    height: '100%',
                    width: '100%',
                  }}
                  resizeMode="cover"
                  source={require('../../assets/images/oxycodone1.png')}
                  //uri: "https://image.cnbcfm.com/api/v1/image/106689818-1599150563582-musk.jpg?v=1620227840",
                />
              </View>
              <View
                style={{
                  marginTop: vh(1),
                  marginLeft: vh(2),
                  //  width: vh(35.83),
                  flexDirection: 'column',
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    // justifyContent: "space-between",
                    //alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text style={styles.txt}>{item.medicineName}</Text>
                  <Text style={styles.txtalt}>20 Pills Left</Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    backgroundColor: '#f3f6f6',
                    width: vw(68),
                    justifyContent: 'space-between',
                    // alignContent: 'space-between',
                  }}>
                  <View style={styles.label}>
                    <View style={{flexDirection: 'row'}}>
                      <Icon name="local-cafe" />
                      {item.beforeBreakFast ? (
                        <Text style={styles.labeltxt}>Before Breakfast</Text>
                      ) : null}
                      {item.afterBreakFast ? (
                        <Text style={styles.labeltxt}>After Breakfast</Text>
                      ) : null}
                    </View>

                    {item.beforeLunch ? (
                      <Text style={styles.labeltxt}>Before Lunch</Text>
                    ) : null}
                    {item.afterLunch ? (
                      <Text style={styles.labeltxt}>After Lunch </Text>
                    ) : null}

                    {item.beforeDinner ? (
                      <Text style={styles.labeltxt}>Before Dinner</Text>
                    ) : null}
                    {item.afterDinner ? (
                      <Text style={styles.labeltxt}>After Dinner</Text>
                    ) : null}
                    {/* <Text style={styles.labeltxt}>Before Breakfast</Text> */}
                  </View>
                  <View style={styles.label}>
                    <View style={{flexDirection: 'row'}}>
                      <Icon name="query-builder" />
                      {item.beforeBreakFast ? (
                        <Text style={styles.labeltxt}>6-7 am Approx</Text>
                      ) : null}
                      {item.afterBreakFast ? (
                        <Text style={styles.labeltxt}>9-10 am Approx</Text>
                      ) : null}
                    </View>
                    {item.beforeLunch ? (
                      <Text style={styles.labeltxt}>12-1 pm Approx</Text>
                    ) : null}
                    {item.afterLunch ? (
                      <Text style={styles.labeltxt}>2-3 pm Approx</Text>
                    ) : null}
                    {item.beforeDinner ? (
                      <Text style={styles.labeltxt}>7-8 pm Approx</Text>
                    ) : null}
                    {item.afterDinner ? (
                      <Text style={styles.labeltxt}>9-10 pm Approx</Text>
                    ) : null}
                  </View>
                  <View style={styles.label}>
                    <Icon name="edit-off" />
                    <Text style={styles.labeltxt}>1 Pill</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
      />
      <ActionSheet ref={actionSheetRef}>
        <MedicineView medInfo={data} selectedItem={selectedItem} />
      </ActionSheet>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height: vh(12),
    backgroundColor: '#fff',
    overflow: 'hidden',
    marginBottom: vh(1.66),
    borderRadius: vw(2),
  },
  imgcont: {width: vh(10)},
  label: {
    backgroundColor: '#f3f6f6',
    flexDirection: 'column',
    marginRight: vh(0.8),
    alignItems: 'center',
    borderRadius: 2,
  },
  labeltxt: {fontSize: vh(1.3), marginLeft: 5, marginTop: 0},
  txt: {color: '#062816', fontSize: vh(2.5)},
  txtalt: {color: '#a4d46e', fontSize: vh(2), marginLeft: vh(1.8)},
});
export default Medicine;
