import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
} from 'react-native-responsive-dimensions';
const BFoodTab = [
  {
    src1: require('../../../assets/images/Component.png'),
    id: 1,
    name: 'Square Concert event',
    date: '10 October 2021',
  },
  {
    src1: require('../../../assets/images/Component.png'),
    id: 1,
    name: 'Square Concert event',
    date: '10 October 2021',
  },
  // {
  //   src1: require('../../../assets/Banner/new.png'),
  //   id: 1,
  //   name: 'Square Concert event',
  //   date: '10 October 2021',
  // },
  // {
  //   src1: require('../../../assets/Banner/new.png'),
  //   id: 1,
  //   name: 'Square Concert event',
  //   date: '10 October 2021',
  // },
];
//const image = {uri: 'https://reactjs.org/logo-og.png'};

function Banner(props) {
  return (
    <View style={{}}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={BFoodTab}
        keyExtractor={(item, index) => index.toString()}
        contentContainerStyle={
          {
            // paddingTop: header ? 20 : -20,
            // marginBottom: 50,
          }
        }
        renderItem={({item}) => {
          return (
            <View
              style={{
                width: responsiveWidth(75),
                height: responsiveHeight(18),
                // marginRight: 20,
                backgroundColor: '#c5e4f6',
                marginRight: 25,
                borderRadius: 10,
              }}>
              <ImageBackground
                source={item.src1}
                style={{
                  width: responsiveWidth(75),
                  borderRadius: 10,
                  height: responsiveHeight(18),
                }}>
                <Text
                  style={{
                    fontSize: responsiveFontSize(2),
                    fontWeight: '800',
                    marginLeft: 10,
                    marginTop: 15,
                  }}>
                  {item.name}
                </Text>

                <Text
                  style={{
                    fontSize: responsiveFontSize(1.5),
                    fontWeight: '800',
                    marginLeft: 10,
                    marginTop: 15,
                  }}>
                  {item.date}
                </Text>

                <TouchableOpacity
                  style={{
                    backgroundColor: '#06112d',
                    width: responsiveWidth(20),
                    height: responsiveHeight(4.5),
                    borderRadius: 5,
                    justifyContent: 'center',
                    marginLeft: 10,
                    marginTop: 10,
                  }}>
                  <Text
                    style={{
                      fontSize: responsiveFontSize(1.4),
                      fontWeight: 'bold',
                      color: 'white',
                      textAlign: 'center',
                    }}>
                    Join Event
                  </Text>
                </TouchableOpacity>
              </ImageBackground>
            </View>
          );
        }}
      />
    </View>
  );
}

export default Banner;
