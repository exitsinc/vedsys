import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  Image,
  ScrollView,
  View,
  FlatList,
} from 'react-native';
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/Ionicons';
import Filter from 'react-native-vector-icons/MaterialCommunityIcons';
import Pill from 'react-native-vector-icons/MaterialCommunityIcons';
import Break from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';

const medicine = [
  {
    id: 1,
    src: require('../../assets/images/bigoxycodone.png'),
    heading: 'Oxycodone',
    pillsLeft: '20 Pills Left',
    subheading: 'Before Breakfast',
    time: '6-7am Approx',
    no_pills: '1 Pill',
    // img: './Images/PlacementArea-29.png',
  },
  {
    id: 2,
    src: require('../../assets/images/bigvitamin.png'),
    heading: 'B 12',
    pillsLeft: '20 Pills Left',
    subheading: 'After Breakfast',
    time: '8-9am Approx',
    no_pills: '1 Pill',
  },
  // {
  //   id: 3,
  //   src: require('../../assets/images/medicine.png'),
  //   heading: 'Vitamin C',
  //   pillsLeft: '20 Pills Left',
  //   subheading: 'After Breakfast',
  //   time: '8-9am Approx',
  //   no_pills: '1 Pill',
  // },
];

function MedicineView(props) {
  const [data, setData] = useState({});
  const {medInfo, selectedItem} = props;
  console.log(`medInfo : ${medInfo} || selectedItem : `, selectedItem);
  //const data = useSelector(reduxState => reduxState.login.medicineDetails);
  // console.log('MedicineDetails from View>>', data);
  // data.map((d, i) => {
  //   return console.log('MedicineName>>>', d.medicineName);
  // });
  useEffect(() => {
    if (medInfo.length > 0) {
      medInfo.map((d, i) => {
        if (d.medicineName == selectedItem.medicineName) {
          setData(d);
        }
      });
    }
  }, []);
  console.log('data imp >> ', data);
  console.log('Schedule Time:', data.afterBreakFast);
  return (
    <View style={styles.container}>
      <View style={{height: '6%'}}>
        <Icon name="caret-down" size={25} />
      </View>

      <View style={styles.namecontainer}>
        <Text style={styles.text}>{data.medicineName}</Text>
        <Text style={{fontSize: responsiveFontSize(2), color: 'green'}}>
          20 Pills Left
        </Text>
      </View>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        // style={{backgroundColor: 'red'}}
        data={medicine}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => (
          <View style={styles.imagecontainer}>
            <View style={styles.imageview}>
              <Image
                source={item.src}
                style={{
                  height: responsiveHeight(25),
                  width: responsiveWidth(70),
                  borderRadius: 25,
                }}
              />
            </View>
          </View>
        )}
      />
      <View style={styles.detailcontainer}>
        <View style={{flexDirection: 'row'}}>
          <Break name="free-breakfast" size={22} color="black" />
          {data.beforeBreakFast ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                marginLeft: 15,
                color: '#000',
              }}>
              Before Breakfast
            </Text>
          ) : null}
          {data.afterBreakFast ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                marginLeft: 15,
                color: '#000',
              }}>
              After Breakfast
            </Text>
          ) : null}
          {data.beforeLunch ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                // marginLeft: 15,
                color: '#000',
              }}>
              , Before Lunch
            </Text>
          ) : null}
          {data.afterLunch ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                // marginLeft: 15,
                color: '#000',
              }}>
              , After Lunch
            </Text>
          ) : null}
          {data.beforeDinner ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                // marginLeft: 15,
                color: '#000',
              }}>
              , Before Dinner
            </Text>
          ) : null}
          {data.afterDinner ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                // marginLeft: 15,
                color: '#000',
              }}>
              , After Dinner
            </Text>
          ) : null}
        </View>

        <View style={{flexDirection: 'row'}}>
          <Filter name="clock-outline" size={22} color="black" />
          {data.beforeBreakFast ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                marginLeft: 15,
                color: '#000',
              }}>
              6-7 am
            </Text>
          ) : null}
          {data.afterBreakFast ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),
                marginLeft: 15,
                color: '#000',
              }}>
              9-10 am
            </Text>
          ) : null}
          {data.beforeLunch ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),

                color: '#000',
              }}>
              , 12-1 pm
            </Text>
          ) : null}
          {data.afterLunch ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),

                color: '#000',
              }}>
              , 2-3 pm
            </Text>
          ) : null}
          {data.beforeDinner ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),

                color: '#000',
              }}>
              , 7-8 pm
            </Text>
          ) : null}
          {data.afterDinner ? (
            <Text
              style={{
                fontSize: responsiveFontSize(1.8),

                color: '#000',
              }}>
              , 9-10 pm
            </Text>
          ) : null}
        </View>
        <View style={{flexDirection: 'row'}}>
          <Pill name="pill" size={22} color="black" />
          <Text
            style={{
              fontSize: responsiveFontSize(2),
              marginLeft: 15,
              color: '#000',
            }}>
            1 Pill
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: responsiveHeight(60),
    width: responsiveWidth(100),
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    alignItems: 'center',
  },
  text: {
    fontSize: responsiveFontSize(2.5),
    fontWeight: 'bold',
  },
  namecontainer: {
    width: responsiveWidth(100),
    height: '15%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'space-between',
  },
  imagecontainer: {
    width: responsiveWidth(80),
    paddingLeft: 30,
  },
  imageview: {
    width: responsiveWidth(50),
    flexDirection: 'row',
    marginRight: 10,
  },
  detailcontainer: {
    width: responsiveWidth(100),
    height: '32%',
    justifyContent: 'space-evenly',
    marginTop: 10,
    paddingLeft: 30,
  },
});

export default MedicineView;
