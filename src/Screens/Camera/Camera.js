import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {RNCamera} from 'react-native-camera';
import {useCamera} from 'react-native-camera-hooks';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {compose} from 'redux';
import CustomButton from '../../Component/CustomButton/CustomButton';
export default function Camera() {
  const [{cameraRef}, {takePicture}] = useCamera(null);
  const captureHandle = async () => {
    try {
      const data = await takePicture;
      console.log(data.uri);
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <View style={styles.bdoy}>
      {/* <View><Text>Hello</Text></View> */}
      <RNCamera
        ref={cameraRef}
        type={RNCamera.Constants.Type.back}
        style={styles.preview}>
        <CustomButton
          title="capture"
          color="#1eb900"
          onPressFunction={() => captureHandle}
        />
      </RNCamera>
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  preview: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
