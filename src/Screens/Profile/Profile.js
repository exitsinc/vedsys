import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import Fun from 'react-native-vector-icons/SimpleLineIcons';
import Pill from 'react-native-vector-icons/MaterialCommunityIcons';
import Help from 'react-native-vector-icons/Feather';
import Reports from 'react-native-vector-icons/FontAwesome5';
import Reminders from 'react-native-vector-icons/MaterialIcons';
import Services from 'react-native-vector-icons/MaterialCommunityIcons';
import Settings from 'react-native-vector-icons/MaterialIcons';
import Logout from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {
  setIsLogged,
  setUser,
  setCategories,
} from '../../Redux/Slice/LoginSlice';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {setProfilePic} from '../../Redux/Slice/LoginSlice';
const call = new ApiCalls();
import ApiCalls from '../../Api/Api';
export default function Profile({navigation}) {
  const [imageSource, setImageSource] = useState(null);
  const dispatch = useDispatch();
  const data = useSelector(reduxState => reduxState.login);
  console.log('User Data>>>', data);
  const {
    user = {
      username: null,
      firstName: null,
    },
  } = data;
  // console.log('UserName>>>>', user.user.firstName);
  console.log('ProfilePIc>>>', data.profilePic);
  let categories = [];
  useEffect(() => {
    console.log('Hello');
    (async () => {
      console.log('Hello>>>>>');
      categories = await call.Calls('category', 'GET');
      console.log('Categories Data>>>>', categories.data.results);
      dispatch(setCategories(categories.data.results));
    })();

    if (data.profilePic != null) {
      console.log('Profile Data>>', data.profilePic);
      setImageSource(data.profilePic);
      return (
        <View
          style={{
            width: 60,
            height: 60,
            borderRadius: 50,
            backgroundColor: 'red',
          }}>
          <Image
            source={{uri: imageSource}}
            style={{width: 60, height: 60, borderRadius: 50}}
            resizeMode="contain"
          />
        </View>
      );
    } else {
      console.log(null);
      return (
        <View
          style={{
            width: 60,
            height: 60,
            borderRadius: 50,
            backgroundColor: 'red',
          }}>
          <Image
            source={{
              uri: 'https://www.kindpng.com/picc/m/495-4952535_create-digital-profile-icon-blue-user-profile-icon.png',
            }}
            style={{width: 60, height: 60, borderRadius: 50}}
          />
        </View>
      );
    }
  }, []);
  {
    imageSource != null
      ? imageSource === data.profilePic
        ? console.log('ProfilePIc Image redux vaalla>>>', data.profilePic)
        : console.log('profileImage set vala>>>', imageSource)
      : console.log(null);
  }
  const logoutHandler = () => {
    console.log('User Logout!!!!');
    //dispatch(setUser([]));
    dispatch(setIsLogged(false));
    navigation.navigate('Home');
  };

  function selectImage() {
    let options = {
      title: 'You can choose one image',
      maxWidth: 256,
      maxHeight: 256,
      noData: true,
      mediaType: 'photo',
      storageOptions: {
        skipBackup: true,
      },
    };

    launchImageLibrary(options, response => {
      console.log('response>>>>', response.assets[0].uri);
      if (response.didCancel) {
        console.log('User cancelled photo picker');
        Alert.alert('You did not select any image');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {uri: response.assets[0].uri};

        // ADD THIS

        console.log('Source of Image>>>>>', source);
        console.log('Source of Image URI>>>>>', source.uri);
        setImageSource(source.uri);
        dispatch(setProfilePic(source.uri));
      }
    });
  }

  return (
    <View style={{backgroundColor: '#C6E5F7', width: '100%', height: '100%'}}>
      <View
        style={{
          margin: 20,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{display: 'flex', flexDirection: 'row'}}>
          <TouchableOpacity onPress={selectImage}>
            {imageSource === null ? (
              <View
                style={{
                  width: 60,
                  height: 60,
                  borderRadius: 50,
                  backgroundColor: 'red',
                }}>
                <Image
                  source={{
                    uri: 'https://www.kindpng.com/picc/m/495-4952535_create-digital-profile-icon-blue-user-profile-icon.png',
                  }}
                  style={{width: 60, height: 60, borderRadius: 50}}
                />
              </View>
            ) : (
              <View
                style={{
                  width: 60,
                  height: 60,
                  borderRadius: 50,
                  backgroundColor: 'red',
                }}>
                <Image
                  source={{uri: imageSource}}
                  style={{width: 60, height: 60, borderRadius: 50}}
                  resizeMode="contain"
                />
              </View>
            )}
          </TouchableOpacity>

          <View style={{flexDirection: 'column'}}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: 'bold',
                color: '#06112D',
                margin: 15,
              }}>
              {user.user.firstName}
              {/* {typeof user.user != undefined && user.user.firstName != undefined
                ? user.user.firstName
                : null} */}
            </Text>
          </View>
        </View>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Help name="help-circle" size={20} />
          <Text style={{fontSize: 15, fontWeight: 'bold', margin: 15}}>
            Help
          </Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: 'column',
          flex: 1,
          justifyContent: 'space-between',
          margin: 10,
          padding: 20,
        }}>
        <View>
          <TouchableOpacity
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Reports name="notes-medical" size={25} />
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                margin: 15,
              }}>
              Reports
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Fun name="game-controller" size={25} color="#000" />
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                margin: 15,
              }}>
              Fun & Games
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Reminders name="event-note" size={25} />

            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                margin: 15,
              }}>
              Reminders
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => navigation.navigate('Services')}>
            <Services name="heart-plus-outline" size={25} />
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                margin: 15,
              }}>
              Services
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              // alignSelf:'center'
            }}>
            <Pill name="pill" size={32} />
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                margin: 15,
              }}>
              Pill Dispense
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{marginLeft: 20, marginBottom: 10}}>
        <View style={{display: 'flex', flexDirection: 'row'}}>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              borderRightWidth: 2,
              borderRadius: 0,
              padding: 5,
              alignItems: 'center',
            }}>
            <Settings name="settings" size={25} />
            <Text style={{fontSize: 15, fontWeight: 'bold', marginLeft: 10}}>
              Settings
            </Text>
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              padding: 5,
              alignItems: 'center',
              marginLeft: 5,
            }}>
            <Logout name="logout" size={25} />
            <TouchableOpacity onPress={() => logoutHandler()}>
              <Text style={{fontSize: 15, fontWeight: 'bold', marginLeft: 10}}>
                Log Out
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

// logouthandler{
//   dispatch(setUser([])
// navigation
// }
