import React, {useState, useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import Card from "./Card";
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
// import CareTackerItems from './CareTackerItems';
import Header from '../../Component/Header/Header';
import NewBottomDemo from '../../Component/NewBottomDemo';
import {useDispatch, useSelector} from 'react-redux';
function CareTacker({navigation, route}) {
  const Care_data = useSelector(
    reduxState => reduxState.services.careTakerService,
  );

  console.log('Caretaker from redux >>>>>>>', Care_data);
  return (
    <SafeAreaView>
      <View style={{backgroundColor: '#FFF', height: '100%'}}>
        <Header navigation={navigation} />
        <ScrollView showsVerticalScrollIndicator>
          <View style={styles.upper}>
            <Text style={{fontSize: vf(2.5), fontWeight: 'bold'}}>
              Care Taker Providers
            </Text>
            <Icon name="filter-alt" size={30} color="#000" />
          </View>
          <View>
            <FlatList
              style={{margin: 5}}
              data={Care_data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (
                <View
                  style={{
                    flexDirection: 'row',
                    margin: 10,
                    //padding: 10,
                    borderColor: '#F3F6F6',
                    borderRadius: 10,
                    borderWidth: 1,
                    justifyContent: 'space-between',
                  }}>
                  <Image
                    source={{uri: item.pic1}}
                    style={{
                      width: 100,
                      height: 100,
                      borderTopLeftRadius: 10,
                      borderBottomLeftRadius: 10,
                    }}
                  />
                  <View
                    style={{
                      margin: 5,
                      flexDirection: 'column',
                    }}>
                    <Text style={{fontSize: vf(2.2), fontWeight: '700'}}>
                      {item.name}
                    </Text>
                    <Text style={{fontSize: vf(1.8)}}>{item.address}</Text>
                    <Text style={{fontSize: vf(1.8)}}>{item.rating}</Text>
                    <Text style={{fontSize: vf(1.8)}}>
                      {item.serviceTime} | {item.serviceType}
                    </Text>
                  </View>
                </View>
              )}
            />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  // container: {margin: 10, marginTop: 30, backgroundColor: '#FFFF'},
  upper: {
    marginLeft: 15,
    //  marginRight: 5,
    margin: 10,
    marginTop: 25,
    backgroundColor: '#FFFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  list: {},
});
export default CareTacker;
