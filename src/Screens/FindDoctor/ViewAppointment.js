import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  Image,
  ScrollView,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  responsiveFontSize as rf,
  responsiveHeight as rh,
  responsiveWidth as rw,
} from 'react-native-responsive-dimensions';
import SelectDropdown from 'react-native-select-dropdown';
//import {useSelector} from 'react-redux';
import {useDispatch, useSelector} from 'react-redux';

import {setappointment} from '../../Redux/Slice/LoginSlice';
const gender = ['Male', 'Female'];
const ViewAppointment = () => {
  const dispatch = useDispatch();
  const [state, setState] = useState({
    symptoms: null,
    appointmentDate: null,
    department: null,
    appointmentTime: null,
    gender: null,
    hospitalName: null,
    doctorName: null,
  });
  console.log('state Appointment >>>', state);
  const appointmentState = useSelector(
    reduxState => reduxState.login.appointment,
  );
  const handleTextChange = (lbl, txt) => {
    console.log('txt', lbl, txt);
    setState({...state, [lbl]: txt});
    dispatch(setappointment({...state, [lbl]: txt}));
  };

  useEffect(() => {
    console.log('USE EFFECT Appointment Details', state, appointmentState);
    setState({...appointmentState});
  }, []);

  useEffect(() => {
    setState({...appointmentState});
    console.log('USE EFFECT Appointment >>>', state, appointmentState);
  }, [appointmentState]);

  const data = useSelector(reduxState => reduxState.login);
  console.log('User Data>>>', data);
  const {
    user = {
      username: null,
      firstName: null,
      lastName: null,
    },
  } = data;
  //console.log('UserName>>>>', user.user.username);
  return (
    <View style={{height: rh(70)}}>
      <ScrollView>
        <View
          style={{
            flexDirection: 'column',
            padding: 10,
          }}>
          <View style={{margin: 10, marginLeft: 25}}>
            {/* <Text style={{fontSize: rf(2.5)}}>Medicine Name</Text> */}
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={user.user.firstName}
              placeholder="Enter Patient Name"
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              placeholder="Enter phone No "
            />

            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={user.user.email}
              placeholder="Enter Email "
            />

            <SelectDropdown
              data={gender}
              defaultButtonText="Select Gender"
              buttonStyle={{marginTop: 10, width: rw(85), borderRadius: 10}}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem, index);
              }}
              onSelect={(selectedItem, index) => {
                console.log(selectedItem, index);
                handleTextChange('gender', selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                // text represented after item is selected
                // if data array is an array of objects then return selectedItem.property to render after item is selected
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                // text represented for each item in dropdown
                // if data array is an array of objects then return item.property to represent item in dropdown
                return item;
              }}
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.symptoms}
              onChangeText={e => {
                handleTextChange('symptoms', e);
              }}
              placeholder="Enter Symptoms "
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.appointmentDate}
              onChangeText={e => {
                handleTextChange('appointmentDate', e);
              }}
              placeholder="Select Date "
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.department}
              onChangeText={e => {
                handleTextChange('department', e);
              }}
              placeholder=" Enter Department "
            />

            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.appointmentTime}
              onChangeText={e => {
                handleTextChange('appointmentTime', e);
              }}
              placeholder=" Enter Time "
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.doctorName}
              onChangeText={e => {
                handleTextChange('doctorName', e);
              }}
              placeholder=" Enter DoctorName "
            />
            <TextInput
              style={{
                color: '#9BA0AB',
                borderBottomColor: '#9B9FAB',
                borderBottomWidth: 1,
                fontSize: rf(1.8),
                width: rw(85),
              }}
              value={state.hospitalName}
              onChangeText={e => {
                handleTextChange('hospitalName', e);
              }}
              placeholder=" Enter HospitalName "
            />
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 10,
            }}>
            <TouchableOpacity
              //   onPress={() => {
              //     gotoMedicineDetails();
              //   }}
              style={{
                backgroundColor: 'black',
                padding: 5,
                width: rw(30),
                borderRadius: 30,
                textAlign: 'center',
                height: rh(7),
                justifyContent: 'center',
                alignSelf: 'center',
                // marginBottom: 10,
              }}>
              <Text
                style={{
                  fontSize: rf(2),
                  color: 'white',
                  textAlign: 'center',
                }}>
                Submit
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ViewAppointment;
