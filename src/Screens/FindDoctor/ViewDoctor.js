import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  View,
} from 'react-native';
import {
  responsiveHeight as vh,
  responsiveWidth as vw,
  responsiveFontSize as vf,
} from 'react-native-responsive-dimensions';
import Icon from 'react-native-vector-icons/MaterialIcons';

const doctorDetails = [
  {
    id: 1,
    src: require('../../assets/Doctor.png'),
    name: 'Dr. Meka Satyanarayana',
    rating: '4.5 Rating',
    type: 'General Physician',
    experience: '11yrs experience',
    address: 'ABC Clinic, Indira nagar 5th Block Bangalore',
  },
];

function ViewDoctor(props) {
  const {data} = props;
  console.log('Props>>', props);

  // const closeSheet = () => {

  // };
  return (
    <FlatList
      data={[data]}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({item}) => (
        <View style={styles.container}>
          <View style={{position: 'absolute', marginTop: vh(1), right: 10}}>
            <TouchableOpacity
            // onPress={closeSheet}
            >
              <Icon name="close" size={25} />
            </TouchableOpacity>
          </View>
          <View style={styles.main}>
            <Image
              source={{uri: item.pic1}}
              resizeMode="contain"
              style={styles.img}
            />

            <View style={styles.txtMain}>
              <Text
                style={{
                  fontSize: vf(2.3),
                  fontWeight: 'bold',
                }}>
                {item.name}
              </Text>
              <Text style={{fontSize: vf(1.8), fontWeight: 'bold'}}>
                {item.rating}
              </Text>
              <Text style={styles.txt}>{item.serviceType}</Text>
              <Text style={styles.txt1}>{item.address}</Text>
            </View>
          </View>
          <View>
            <Text
              style={{
                fontSize: vf(2.3),
                fontWeight: 'bold',
                marginTop: vh(1.5),
              }}>
              Reviews
            </Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>John Doe | 4.5 Rating</Text>
            <Text style={{fontSize: vf(1.4)}}>
              Your Practice is great. The services you provide are incredible
              and the patient experience you provide is like nothing else.
            </Text>
            <Text style={{fontWeight: 'bold'}}>Like | Dislike</Text>
          </View>
          <View>
            <Text style={{fontWeight: 'bold'}}>John Doe | 4.5 Rating</Text>
            <Text style={{fontSize: vf(1.4)}}>
              Your Practice is great. The services you provide are incredible
              and the patient experience you provide is like nothing else.
            </Text>
            <Text style={{fontWeight: 'bold'}}>Like | Dislike</Text>
          </View>
        </View>
      )}
    />
  );
}
const styles = StyleSheet.create({
  btn: {
    height: vh(4.33),
    width: vh(24.16),
    backgroundColor: '#06112d',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: vh(1.66),
    borderRadius: vw(4),
  },
  container: {
    backgroundColor: '#fff',
    width: vw(100),
    height: vh(55),
    borderTopLeftRadius: vw(6),
    borderTopRightRadius: vw(6),
    paddingHorizontal: vh(2.66),
    paddingTop: vh(3.33),
  },
  img: {height: vh(16.16), width: vh(12.33)},
  // img: {height: 100, width: 100},
  main: {flexDirection: 'row'},
  txt: {fontSize: vf(1.8)},
  txt1: {fontSize: vf(1.8), width: 250},
  txtMain: {marginLeft: vh(1.66), marginTop: 0},
});
export default ViewDoctor;
