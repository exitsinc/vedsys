import React from 'react'

import {createStackNavigator} from '@react-navigation/stack';
import Home from '../../Screens/HomeScreen/HomeScreen';
import Screen from '../../Screens/lastscreen/Screen';
import FindDoctor from '../../Screens/FindDoctor/FindDoctor';
import Services from '../../Screens/Services/Services';

const Stack = createStackNavigator();
export default function ServiceStack() {
    return (
        // define here all Home tabs route here
        <Stack.Navigator initialRouteName="HomeScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="HomeScreen" component={Home} /> 
        <Stack.Screen name="Screen" component={Screen} /> 
        <Stack.Screen name="Services" component={Services} />
      </Stack.Navigator>
    )
}
