import React from 'react';
import {Image, Text, View} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {Avatar} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Feather';
// import { useDispatch } from 'react-redux';
// import { setIsLogged } from '../../Redux/Slice/LoginSlice';
export default function DrawerContent({navigation}) {
  // const dispatch = useDispatch()
  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <View style={{alignSelf: 'center', marginTop: 80}}>
            <Avatar.Image
              size={110}
              source={{
                uri: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
              }}
            />
          </View>
          {/* <View style={{marginTop: 10}}>
          <Image
             source={{uri:'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'}}
    
            style={{width: 100, height: 100, borderRadius: 50}}
          />
          </View> */}
          <View style={{marginTop: 10}}>
            <Text
              style={{alignSelf: 'center', fontSize: 20, fontWeight: 'bold'}}>
              Xavier
            </Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text
              style={{alignSelf: 'center', fontSize: 14, fontWeight: 'bold'}}>
              xavier@gmail.com
            </Text>
          </View>
          {/* <View style={{marginTop: 10, borderWidth: 0.5}}></View> */}
          <View style={{marginTop: 15}}>
            <TouchableOpacity>
              <Icon
                style={{paddingLeft: 10}}
                name="home"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Home
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity onPress={() => navigation.navigate('Routine')}>
              <Icon
                style={{paddingLeft: 10}}
                name="search"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Routine
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity>
              <Icon
                style={{paddingLeft: 10}}
                name="activity"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Health
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 15}}>
            <TouchableOpacity onPress={() => navigation.navigate('Chat')}>
              <Icon
                style={{paddingLeft: 10}}
                name="info"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Chat
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity>
              <Icon
                style={{paddingLeft: 10}}
                name="message-circle"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Feedback
              </Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 15}}>
            <TouchableOpacity
              onPress={() => navigation.navigate('ContactDetails')}>
              <Icon
                style={{paddingLeft: 10}}
                name="phone"
                size={25}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Contact Us
              </Text>
            </TouchableOpacity>
          </View>
          {/* <View style={{marginTop: 10, borderWidth: 0.5}}></View> */}

          <View
            style={{
              marginTop: 15,
              display: 'flex',
              justifyContent: 'flex-end',
            }}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
              <Icon
                style={{paddingLeft: 10}}
                name="log-out"
                size={20}
                color="#9CD161"
              />
              <Text
                style={{
                  position: 'absolute',
                  paddingLeft: 45,
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                Logout
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
